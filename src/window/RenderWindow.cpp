#include <cstring>
#include <sstream>

#include "core/Exceptions.hpp"
#include "core/Logger.hpp"
#include "window/RenderWindow.hpp"

#include "Tracy.hpp"
namespace BlueSnow
{
RenderWindow::RenderWindow() {}

////////////////////////////////////////////////////////////////////////////////

RenderWindow::RenderWindow(const WindowSettings &settings) { create(settings); }

////////////////////////////////////////////////////////////////////////////////

void RenderWindow::create(const WindowSettings &settings)
{
    ZoneScopedN("Window create");

    // Make sure the window size > 0
    assert(settings.resolution.size_x > 0 && settings.resolution.size_y > 0);

    // Make sure OpenGL version is at least 3.3
    assert(settings.context_version_major >= 3 &&
           settings.context_version_minor >= 3);

    // Initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        LOG(ERROR) << "Could not init SDL!";
        exit(-1);
    }

    SDL_GL_LoadLibrary(NULL);

    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,
                        settings.context_version_major);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,
                        settings.context_version_minor);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

    // Create the window
    if (settings.fullscreen)
    {
        m_window =
            SDL_CreateWindow(settings.title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                             SDL_WINDOWPOS_UNDEFINED, 0, 0,
                             SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_OPENGL);
    }
    else
    {
        m_window = SDL_CreateWindow(
            settings.title.c_str(), SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED, settings.resolution.size_x,
            settings.resolution.size_y,
            SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_MAXIMIZED);
    }
    if (m_window == NULL)
    {
        LOG(ERROR) << "Could not create window!";
        exit(-1);
    }

    m_gl_context = SDL_GL_CreateContext(m_window);
    if (m_gl_context == nullptr)
    {
        LOG(ERROR) << "Could not create GL context!";
        exit(-1);
    }

    if (settings.vsync)
        SDL_GL_SetSwapInterval(1);
    else
        SDL_GL_SetSwapInterval(-1);

    LOG(DEBUG) << "VSYNC: " << SDL_GL_GetSwapInterval();
}

////////////////////////////////////////////////////////////////////////////////
Event RenderWindow::poll_events()
{
    ZoneScopedN("Poll_Events");
    SDL_Event ev;
    SDL_PollEvent(&ev);
    switch (ev.type)
    {
    case SDL_QUIT:
    {
        Event return_event;
        return_event.type = BLUESNOW_QUIT;
        return return_event;
        break;
    }
    case SDL_WINDOWEVENT:
    {
        switch (ev.window.event)
        {
        case SDL_WINDOWEVENT_SIZE_CHANGED:
        {
            Event return_event;
            return_event.type = BLUESNOW_WINDOW;
            return_event.window.type = BLUESNOW_WINDOW_RESIZE;
            return_event.window.size = {ev.window.data1, ev.window.data2};
            return return_event;
            break;
        }
        }
        break;
    }
    default:
    {
        Event return_event;
        return_event.type = BLUESNOW_NONE;
        return return_event;
        break;
    }
    }
}

////////////////////////////////////////////////////////////////////////////////

void RenderWindow::close_window() { SDL_DestroyWindow(m_window); }

////////////////////////////////////////////////////////////////////////////////

void RenderWindow::swap_buffers()
{
    ZoneScopedN("Swap");
    SDL_GL_SwapWindow(m_window);
}

////////////////////////////////////////////////////////////////////////////////

RenderWindow::~RenderWindow() { close_window(); }
} // namespace BlueSnow