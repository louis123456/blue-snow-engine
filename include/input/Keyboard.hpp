#pragma once

#include "input/KeyCodes.hpp"

namespace BlueSnow
{
namespace Keyboard
{

/**
 * \brief Is a key pressed oj the keybard
 *
 * \param Key The key to check /ref KeyCode
 * \return true Yes
 * \return false No
 */
bool is_key_down(KeyCode Key);

} // namespace Keyboard

} // namespace BlueSnow
