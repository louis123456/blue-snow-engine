.. BlueSnowEngine documentation master file, created by
   sphinx-quickstart on Sun Dec 22 16:50:33 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BlueSnowEngine's documentation!
==========================================

.. include:: loc.rst

.. toctree::
   :maxdepth: 4

   planning/engine-planning
   planning/research
   planning/api/examples
   planning/api/components
   planning/material_variables

   api/library_root
