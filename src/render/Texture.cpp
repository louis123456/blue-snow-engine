#include "render/Texture.hpp"

namespace BlueSnow
{
////////////////////////////////////////////////////////////////////////////////

Texture::Texture() {}

////////////////////////////////////////////////////////////////////////////////

Texture::Texture(const std::string &path, const TextureSettings &settings)
{
    load(path, settings);
}

////////////////////////////////////////////////////////////////////////////////

void Texture::load(const std::string &path, const TextureSettings &settings)
{
    LOG(DEBUG) << "Loading texture " << path;
    glGenTextures(1, &m_texture_id);
    glBindTexture(GL_TEXTURE_2D, m_texture_id);

    switch (settings.texture_wrap_side)
    {
    case TEXTURE_WRAP_REPEAT:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        break;
    }

    switch (settings.texture_wrap_top)
    {
    case TEXTURE_WRAP_REPEAT:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        break;
    }

    switch (settings.texture_filter_mode_min)
    {
    case TEXTURE_FILTER_LINEAR:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        break;
    case TEXTURE_FILTER_NEAREST:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        break;
    case TEXTURE_MIPMAP_LINEAR:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                        GL_LINEAR_MIPMAP_LINEAR);
        break;
    case TEXTURE_MIPMAP_NEAREST:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                        GL_NEAREST_MIPMAP_LINEAR);
        break;
    }

    switch (settings.texture_filter_mode_mag)
    {
    case TEXTURE_FILTER_LINEAR:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        break;
    case TEXTURE_FILTER_NEAREST:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        break;
    default:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        break;
    }

    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *data =
        stbi_load(path.c_str(), &width, &height, &nrChannels, 0);
    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
                     GL_UNSIGNED_BYTE, data);
        if (settings.genmipmap)
            glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        LOG(ERROR) << "Cannot open image file: " << path;
        exit(-1);
    }
    stbi_image_free(data);
}

////////////////////////////////////////////////////////////////////////////////

void Texture::bind(int slot)
{
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D, m_texture_id);
}

////////////////////////////////////////////////////////////////////////////////

void Texture::unload() { glDeleteTextures(1, &m_texture_id); }

////////////////////////////////////////////////////////////////////////////////

} // namespace BlueSnow
