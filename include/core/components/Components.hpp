#pragma once

#include "ComponentCamera.hpp"
#include "ComponentMesh.hpp"
#include "ComponentModel.hpp"
#include "ComponentRigidBody.hpp"
#include "ComponentScript.hpp"
#include "ComponentTransform.hpp"

namespace BlueSnow
{
enum ComponentType
{
    Unknown = -1,
    Camera,
    Mesh,
    Model,
    RigidBody,
    Script
};

inline ComponentType string_to_component(const std::string &name)
{
    if (name == "Camera")
        return Camera;
    if (name == "Mesh")
        return Mesh;
    if (name == "Model")
        return Model;
    if (name == "RigidBody")
        return RigidBody;
    if (name == "Script")
        return Script;

    return Unknown;
}

} // namespace BlueSnow
