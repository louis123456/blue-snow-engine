#pragma once

#include <chrono>
#include <fstream>
#include <string>

#include <glad/glad.h>
#include <json.hpp>

#include "core/Logger.hpp"
#include "core/Transform.hpp"
#include "core/components/Component.hpp"
#include "core/components/ComponentMesh.hpp"
#include "io/BinaryBuffer.hpp"
#include "io/BufferView.hpp"

namespace BlueSnow
{
class ComponentModel : public Component
{
private:
    std::vector<std::shared_ptr<ComponentMesh>> m_mesh_data;

    std::vector<BinaryBuffer> m_buffers;
    std::vector<BufferView> m_buffer_views;

    nlohmann::json m_file_json_data;

    void load_gltf(const std::string &path);

    void load_json_chunk(const std::vector<unsigned char> &data);
    void load_binary_chunk(const std::vector<unsigned char> &data);

    void load_meshes();

public:
    ComponentModel();

    explicit ComponentModel(const std::string &data) { load(data); }

    /**
     * \brief Clones the Component
     *
     * \return
     */
    ComponentModel *clone() const { return new ComponentModel(*this); }

    /**
     * \brief Get the type
     *
     * \return std::string
     */
    std::string get_type() { return "Model"; }

    /**
     * \brief Loads all data from json passed into data
     *
     * \param data The json string to load from
     */
    void load(const std::string &data);

    /**
     * \brief Unload all data
     *
     */
    void unload();
    /**
     * \brief Called on startup
     *
     */
    void setup();

    /**
     * \brief Called before every frame.
     *
     * \param delta_time Time passed since last call
     */
    void update(std::chrono::microseconds delta_time);

    /**
     * \brief Draw the frame.
     *
     */
    void frame() const;

    /**
     * \brief Called after each frame.
     *
     * \param delta_time
     */
    void post_update(std::chrono::microseconds delta_time);

    /**
     * \brief Creates a json string with Object data.
     *
     */
    std::string save() const;

    /**
     * \brief Called at a constant rate.
     *
     */
    void tick();

    ~ComponentModel();
};
} // namespace BlueSnow
