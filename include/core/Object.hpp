#pragma once

#include <memory>
#include <string>
#include <vector>

#include "components/Components.hpp"
#include "core/Transform.hpp"

namespace BlueSnow
{
class Scene;
class Object
{
private:
    std::vector<std::shared_ptr<Component>> m_components;

    std::string m_name;

    bool m_loaded = false;

    Scene *m_parent;

public:
    /**
     * \brief Constructs a new Object.
     *
     */
    Object();

    /**
     * \brief Coppy constructor
     *
     */
    explicit Object(const Object &obj);

    /**
     * \brief Constructs a new Object.
     *
     */
    explicit Object(const std::string &path) { load(path); }

    /**
     * \brief Set the parent of the component
     *
     * \param parent
     */
    virtual void set_parent(Scene *parent) { m_parent = parent; }

    /**
     * \brief Gets the parent object of the component
     *
     * \return std::weak_ptr<Scene>
     */
    virtual Scene *get_parent() { return m_parent; }

    /**
     * \brief Gets the transform
     *
     * \return std::shared_ptr<ComponentTransform>
     */
    virtual std::shared_ptr<ComponentTransform> get_transform()
    {
        return std::static_pointer_cast<ComponentTransform>(m_components[0]);
    };

    /**
     * \brief Sets the name
     *
     * \param name
     */
    virtual void set_name(const std::string &name) { m_name = name; }

    /**
     * \brief Gets the name
     *
     * \return const std::string&
     */
    virtual const std::string &get_name() const { return m_name; }

    /**
     * \brief Load a object from file
     *
     * \param path The json files path
     */
    virtual void load(const std::string &path);

    /**
     * \brief Unloads all data
     *
     */
    virtual void unload();

    /**
     * \brief Called on startup
     *
     */
    virtual void setup();

    /**
     * \brief Called before every frame.
     *
     * \param delta_time Time passed since last call
     */
    virtual void update(std::chrono::microseconds delta_time);

    /**
     * \brief Draws the frame.
     *
     */
    virtual void frame() const;

    /**
     * \brief Called after each frame.
     *
     * \param delta_time Time passed since last call
     */
    virtual void post_update(std::chrono::microseconds delta_time);

    /**
     * \brief Called at a constant rate
     *
     */
    virtual void tick();

    /**
     * \brief Creates a json string with the object data
     *
     */
    virtual std::string save() const;

    /**
     * \brief Destroys the Object
     *
     */
    virtual ~Object();
};
} // namespace BlueSnow
