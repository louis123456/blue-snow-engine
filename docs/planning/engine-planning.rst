Rough idea of the engine
========================

Core systems
------------

Core
~~~~

The “Core” of the engine, responsible for linking different components.


-  OpenGLInit -> GL setup Code
-  Scene -> A 'World' containing all of the objects inside it
-  Camera -> Camera in the world.
-  Point light
-  Sun light
-  Material -> A material that consists of a shader and textures
-  Component -> Parts of a object

   -  ComponentMeshBase -> Mesh represented as vertices + material

      -  ComponentMesh -> Mesh represented as vertices + material
      -  ComponentAnimatedMesh -> Animated mesh represented as vertices
         + bones + material

   -  ComponentSoundPlayer -> A soundtree player component
   -  ComponentMusicPlayer -> A music player object
   -  ComponentListener -> The player’s ‘ears’
   -  ComponentScript -> A script that can do things to a
      object
   -  ComponentNetworking -> How the object serializes and deserializes.
   -  ComponentPhysicsBase -> A Physics component

      -  ComponentPhysicsRidgedBody -> A physics RidgedBody
      -  …

-  Object -> Object in the game world

   -  ObjectStatic -> A object that cannot move in-game
   -  ObjectDynamic -> A object that can move in-game.

Renderer
~~~~~~~~

-  Shader -> Shader base

   -  ShaderPBR -> Epic/Disney PBRShader
   -  ShaderRayMach -> Raymarching shader.
   -  ShaderVolumetric -> Volumetric effect base

      -  ShaderVolumetricCloud -> Volumetric cloud effect
      -  ShaderVolumetricSmoke -> …
      -  ShaderVolumetricDust -> …
      -  …

   -  ShaderCustom -> Custom Shader

FileIO
~~~~~~

System to load and save files.

-  Shader loader (vs, fs, gs)
-  Image loader (.png, …)
-  Binary manipulation (.bin)
-  Save Game loader/saver (.bin)
-  Audio loader (Vorbis .ogg)
-  Animation loader (glTF)
-  Model loader (glTF)
-  Script loader (.lua)

Networking
~~~~~~~~~~

The interaction of the game with the server/webservers.

-  Winsock
-  Posix sockets
-  HTTP

Audio
~~~~~

Sound system to play sound and music.

-  OpenALInit OpenAL -> Init code
-  Listener -> A listener object
-  SoundBase -> A interface to control sounds

   -  Sound -> Audio loaded entirely into memory
   -  Music -> Audio streamed from the disk.
   -  SoundTree -> A Sound Object that has
      multiple sound files that can be played independently

AI
~~

A way for non-player controlled characters to interact with the game
world.

TODO
