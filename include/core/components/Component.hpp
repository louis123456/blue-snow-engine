#pragma once

#include <chrono>
#include <string>

#include "core/Transform.hpp"

namespace BlueSnow
{
class Object;
class Component
{
protected:
    std::string m_name = "";

    Object *m_parent;

public:
    /**
     * \brief Set the parent of the component
     *
     * \param parent
     */
    virtual void set_parent(Object *parent) { m_parent = parent; }

    /**
     * \brief Get the parent object of the component
     *
     * \return Object*
     */
    virtual Object *get_parent() { return m_parent; }

    /**
     * \brief Sets the name
     *
     * \param name
     */
    virtual void set_name(const std::string &name) { m_name = name; }

    /**
     * \brief Gets the name
     *
     * \return const std::string&
     */
    virtual const std::string &get_name() const { return m_name; }

    /**
     * \brief Clones the Component
     *
     * \return
     */
    virtual Component *clone() const = 0;

    /**
     * \brief Get the type
     *
     * \return std::string
     */
    virtual std::string get_type() = 0;

    /**
     * \brief Loads all data from json passed into data
     *
     * \param data The json string to load from
     */
    virtual void load(const std::string &data) = 0;

    /**
     * \brief Unload all data
     *
     */
    virtual void unload() = 0;
    /**
     * \brief Called on startup
     *
     */
    virtual void setup() = 0;

    /**
     * \brief Called before every frame.
     *
     * \param delta_time Time passed since last call
     */
    virtual void update(std::chrono::microseconds delta_time) = 0;

    /**
     * \brief Draw the frame.
     *
     */
    virtual void frame() const = 0;

    /**
     * \brief Called after each frame.
     *
     * \param delta_time Time passed since last call
     */
    virtual void post_update(std::chrono::microseconds delta_time) = 0;

    /**
     * \brief Creates a json string with Object data.
     *
     */
    virtual std::string save() const = 0;

    /**
     * \brief Called at a constant rate.
     *
     */
    virtual void tick() = 0;

    /**
     * \brief Destroys the Component
     *
     */
    virtual ~Component();
};
} // namespace BlueSnow
