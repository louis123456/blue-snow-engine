#include <iostream>
#include <thread>

#include <glad/glad.h>
#include <json.hpp>

#include "core/GlobalSettings.hpp"
#include "core/Logger.hpp"
#include "core/Object.hpp"
#include "core/RenderMatrices.hpp"

namespace BlueSnow
{
Object::Object() {}

////////////////////////////////////////////////////////////////////////////////

Object::Object(const Object &obj)
{
    m_loaded = obj.m_loaded;
    m_name = obj.m_name;
    m_parent = obj.m_parent;

    for (auto comp : obj.m_components)
    {
        m_components.push_back(std::shared_ptr<Component>(comp->clone()));
        m_components.back()->set_parent(this);
    }
}

////////////////////////////////////////////////////////////////////////////////

void Object::load(const std::string &path)
{
    std::ifstream file(GlobalSettings::get().working_dir + path);
    nlohmann::json json_data;
    file >> json_data;
    file.close();

    m_name = json_data["object"]["name"];

    LOG(DEBUG) << "Loading object " << m_name << ".";

    std::shared_ptr<ComponentTransform> transform(
        new ComponentTransform(json_data["object"]["transform"].dump()));

    transform->set_parent(this);

    m_components.push_back(transform);

    for (nlohmann::json component_data : json_data["object"]["components"])
    {
        LOG(DEBUG) << "Loading component " << component_data["name"];

        std::shared_ptr<Component> comp;

        switch (string_to_component(component_data["type"]))
        {
        case Camera:
            comp = std::make_shared<ComponentCamera>(component_data.dump());
            break;

        case Model:
            comp = std::make_shared<ComponentModel>(component_data.dump());
            break;

        case Mesh:
            comp = std::make_shared<ComponentMesh>(component_data.dump());
            break;

        case RigidBody:
            comp = std::make_shared<ComponentRigidBody>(component_data.dump());
            break;

        case Script:
            comp = std::make_shared<ComponentScript>(component_data.dump());
            break;

        default:
            LOG(ERROR) << "Could not find component type "
                       << component_data["type"];
            comp = nullptr;
            break;
        }

        if (!comp)
            continue;

        comp->set_parent(this);
        m_components.push_back(comp);
        LOG(DEBUG) << "Finished loading component " << component_data["name"];
    }

    LOG(DEBUG) << "Finished loading " << m_name << ".";
    m_loaded = true;
}

////////////////////////////////////////////////////////////////////////////////

void Object::unload()
{
    if (m_loaded)
    {
        LOG(DEBUG) << "Unloading object " << m_name;
        m_components.clear();
        m_loaded = false;
    }
}

////////////////////////////////////////////////////////////////////////////////

void Object::setup()
{
    for (std::shared_ptr<Component> comp : m_components)
    {
        comp->setup();
    }
}

////////////////////////////////////////////////////////////////////////////////

void Object::update(std::chrono::microseconds delta_time)
{
    for (std::shared_ptr<Component> comp : m_components)
    {
        comp->update(delta_time);
    }
}

////////////////////////////////////////////////////////////////////////////////

void Object::frame() const
{
    for (std::shared_ptr<Component> comp : m_components)
    {
        comp->frame();
    }
    RenderMatrices::get().Object = glm::mat4(1.0f);
}

////////////////////////////////////////////////////////////////////////////////

void Object::post_update(std::chrono::microseconds delta_time)
{
    for (std::shared_ptr<Component> comp : m_components)
    {
        comp->post_update(delta_time);
    }
}

////////////////////////////////////////////////////////////////////////////////

void Object::tick()
{
    for (std::shared_ptr<Component> comp : m_components)
    {
        comp->tick();
    }
}

////////////////////////////////////////////////////////////////////////////////

std::string Object::save() const {}

////////////////////////////////////////////////////////////////////////////////

Object::~Object() { unload(); }
} // namespace BlueSnow
