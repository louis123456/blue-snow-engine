#pragma once

#include <string>

namespace BlueSnow
{
class GlobalSettings
{
private:
public:
    GlobalSettings() {}
    GlobalSettings(GlobalSettings const &) = delete;
    void operator=(GlobalSettings const &) = delete;

    std::string working_dir;
    std::string settings_dir;
    std::string name;

    static GlobalSettings &get()
    {
        static GlobalSettings inst;
        return inst;
    }
};
} // namespace BlueSnow
