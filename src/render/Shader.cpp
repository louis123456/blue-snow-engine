#include <cstdlib>
#include <fstream>
#include <iostream>

#include "core/Logger.hpp"
#include "render/Shader.hpp"

namespace BlueSnow
{
Shader::Shader() {}

////////////////////////////////////////////////////////////////////////////////

Shader::Shader(const std::string &vertex, const std::string &fragment)
    : m_vertex_source(vertex), m_fragment_source(fragment)
{
}

////////////////////////////////////////////////////////////////////////////////

bool Shader::load_file(const std::string &vertex, const std::string &fragment)
{
    LOG(DEBUG) << "Reading vertex shader file: " << vertex;
    std::ifstream vertex_file(vertex);
    LOG(DEBUG) << "Reading fragment shader file: " << fragment;
    std::ifstream fragment_file(fragment);

    if (!vertex_file)
    {
        LOG(ERROR) << "Could not read file: " << vertex;
        exit(-1);
    }
    if (!fragment_file)
    {
        LOG(ERROR) << "Could not read file: " << fragment;
        exit(-1);
    }

    m_vertex_source = std::string((std::istreambuf_iterator<char>(vertex_file)),
                                  std::istreambuf_iterator<char>());

    m_fragment_source =
        std::string((std::istreambuf_iterator<char>(fragment_file)),
                    std::istreambuf_iterator<char>());

    return true;
}

////////////////////////////////////////////////////////////////////////////////

bool Shader::compile_and_link() { return compile() && link(); }

////////////////////////////////////////////////////////////////////////////////

bool Shader::compile()
{
    LOG(DEBUG) << "Compiling vertex shader";
    m_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    const char *vert_src = m_vertex_source.c_str();
    glShaderSource(m_vertex_shader, 1, &vert_src, NULL);
    glCompileShader(m_vertex_shader);

    int success;
    char infoLog[512];
    glGetShaderiv(m_vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(m_vertex_shader, 512, NULL, infoLog);
        LOG(ERROR) << "Vertex shader compilation failed:\n" << infoLog;
        exit(-1);
    }

    LOG(DEBUG) << "Compiling fragment shader";
    m_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    const char *frag_src = m_fragment_source.c_str();
    glShaderSource(m_fragment_shader, 1, &frag_src, NULL);
    glCompileShader(m_fragment_shader);

    glGetShaderiv(m_fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(m_fragment_shader, 512, NULL, infoLog);
        LOG(ERROR) << "Fragment shader compilation failed:\n" << infoLog;
        exit(-1);
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////

int Shader::get_shader() const { return m_shader_program; }

////////////////////////////////////////////////////////////////////////////////

bool Shader::link()
{
    m_shader_program = glCreateProgram();
    glAttachShader(m_shader_program, m_vertex_shader);
    glAttachShader(m_shader_program, m_fragment_shader);
    glLinkProgram(m_shader_program);

    int success;
    glGetProgramiv(m_shader_program, GL_LINK_STATUS, &success);
    char infoLog[512];
    if (!success)
    {
        glGetProgramInfoLog(m_shader_program, 512, NULL, infoLog);
        LOG(ERROR) << "Shader linking failed\n" << infoLog;
        exit(-1);
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////

void Shader::use() { glUseProgram(m_shader_program); }

////////////////////////////////////////////////////////////////////////////////

Shader::~Shader()
{
    glDeleteShader(m_vertex_shader);
    glDeleteShader(m_fragment_shader);
    glDeleteProgram(m_shader_program);
}
} // namespace BlueSnow
