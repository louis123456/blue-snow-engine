namespace BlueSnow
{
namespace Mouse
{

enum MouseButton
{
    Left,
    Right,
    Middle
};

} // namespace Mouse
} // namespace BlueSnow