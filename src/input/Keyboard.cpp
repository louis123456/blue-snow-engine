#include <SDL2/SDL.h>

#include "input/Keyboard.hpp"

namespace BlueSnow
{
namespace Keyboard
{
bool is_key_down(KeyCode key)
{
    const Uint8 *keyboard_state = SDL_GetKeyboardState(NULL);

    SDL_Scancode check_key;
    switch (key)
    {
    case A:
        check_key = SDL_SCANCODE_A;
        break;
    case B:
        check_key = SDL_SCANCODE_B;
        break;
    case C:
        check_key = SDL_SCANCODE_C;
        break;
    case D:
        check_key = SDL_SCANCODE_D;
        break;
    case E:
        check_key = SDL_SCANCODE_E;
        break;
    case F:
        check_key = SDL_SCANCODE_F;
        break;
    case G:
        check_key = SDL_SCANCODE_G;
        break;
    case H:
        check_key = SDL_SCANCODE_H;
        break;
    case I:
        check_key = SDL_SCANCODE_I;
        break;
    case J:
        check_key = SDL_SCANCODE_J;
        break;
    case K:
        check_key = SDL_SCANCODE_K;
        break;
    case L:
        check_key = SDL_SCANCODE_L;
        break;
    case M:
        check_key = SDL_SCANCODE_M;
        break;
    case N:
        check_key = SDL_SCANCODE_N;
        break;
    case O:
        check_key = SDL_SCANCODE_O;
        break;
    case P:
        check_key = SDL_SCANCODE_P;
        break;
    case Q:
        check_key = SDL_SCANCODE_Q;
        break;
    case R:
        check_key = SDL_SCANCODE_R;
        break;
    case S:
        check_key = SDL_SCANCODE_S;
        break;
    case T:
        check_key = SDL_SCANCODE_T;
        break;
    case U:
        check_key = SDL_SCANCODE_U;
        break;
    case V:
        check_key = SDL_SCANCODE_V;
        break;
    case W:
        check_key = SDL_SCANCODE_W;
        break;
    case X:
        check_key = SDL_SCANCODE_X;
        break;
    case Y:
        check_key = SDL_SCANCODE_Y;
        break;
    case Z:
        check_key = SDL_SCANCODE_Z;
        break;
    case Num0:
        check_key = SDL_SCANCODE_0;
        break;
    case Num1:
        check_key = SDL_SCANCODE_1;
        break;
    case Num2:
        check_key = SDL_SCANCODE_2;
        break;
    case Num3:
        check_key = SDL_SCANCODE_3;
        break;
    case Num4:
        check_key = SDL_SCANCODE_4;
        break;
    case Num5:
        check_key = SDL_SCANCODE_5;
        break;
    case Num6:
        check_key = SDL_SCANCODE_6;
        break;
    case Num7:
        check_key = SDL_SCANCODE_7;
        break;
    case Num8:
        check_key = SDL_SCANCODE_8;
        break;
    case Num9:
        check_key = SDL_SCANCODE_9;
        break;
    case Escape:
        check_key = SDL_SCANCODE_ESCAPE;
        break;
    case LControl:
        check_key = SDL_SCANCODE_LCTRL;
        break;
    case LShift:
        check_key = SDL_SCANCODE_LSHIFT;
        break;
    case LAlt:
        check_key = SDL_SCANCODE_LALT;
        break;
    case LSystem:
        check_key = SDL_SCANCODE_L;
        break;
    case RControl:
        check_key = SDL_SCANCODE_RCTRL;
        break;
    case RShift:
        check_key = SDL_SCANCODE_RSHIFT;
        break;
    case RAlt:
        check_key = SDL_SCANCODE_RALT;
        break;
    case RSystem:
        check_key = SDL_SCANCODE_R;
        break;
    case Menu:
        check_key = SDL_SCANCODE_MENU;
        break;
    case LBracket:
        check_key = SDL_SCANCODE_LEFTBRACKET;
        break;
    case RBracket:
        check_key = SDL_SCANCODE_RIGHTBRACKET;
        break;
    case Semicolon:
        check_key = SDL_SCANCODE_SEMICOLON;
        break;
    case Comma:
        check_key = SDL_SCANCODE_COMMA;
        break;
    case Period:
        check_key = SDL_SCANCODE_PERIOD;
        break;
    case Quote:
        check_key = SDL_SCANCODE_APOSTROPHE;
        break;
    case Slash:
        check_key = SDL_SCANCODE_SLASH;
        break;
    case Backslash:
        check_key = SDL_SCANCODE_BACKSLASH;
        break;
    case Tilde:
        check_key = SDL_SCANCODE_GRAVE;
        break;
    case Equal:
        check_key = SDL_SCANCODE_EQUALS;
        break;
    case Hyphen:
        check_key = SDL_SCANCODE_MINUS;
        break;
    case Space:
        check_key = SDL_SCANCODE_SPACE;
        break;
    case Enter:
        check_key = SDL_SCANCODE_RETURN;
        break;
    case Backspace:
        check_key = SDL_SCANCODE_BACKSPACE;
        break;
    case Tab:
        check_key = SDL_SCANCODE_TAB;
        break;
    case PageUp:
        check_key = SDL_SCANCODE_PAGEUP;
        break;
    case PageDown:
        check_key = SDL_SCANCODE_PAGEDOWN;
        break;
    case End:
        check_key = SDL_SCANCODE_END;
        break;
    case Home:
        check_key = SDL_SCANCODE_HOME;
        break;
    case Insert:
        check_key = SDL_SCANCODE_INSERT;
        break;
    case Delete:
        check_key = SDL_SCANCODE_DELETE;
        break;
    case Add:
        check_key = SDL_SCANCODE_KP_PLUS;
        break;
    case Subtract:
        check_key = SDL_SCANCODE_MINUS;
        break;
    case Multiply:
        check_key = SDL_SCANCODE_KP_MULTIPLY;
        break;
    case Divide:
        check_key = SDL_SCANCODE_KP_DIVIDE;
        break;
    case Left:
        check_key = SDL_SCANCODE_LEFT;
        break;
    case Right:
        check_key = SDL_SCANCODE_RIGHT;
        break;
    case Up:
        check_key = SDL_SCANCODE_UP;
        break;
    case Down:
        check_key = SDL_SCANCODE_DOWN;
        break;
    case Numpad0:
        check_key = SDL_SCANCODE_KP_0;
        break;
    case Numpad1:
        check_key = SDL_SCANCODE_KP_1;
        break;
    case Numpad2:
        check_key = SDL_SCANCODE_KP_2;
        break;
    case Numpad3:
        check_key = SDL_SCANCODE_KP_3;
        break;
    case Numpad4:
        check_key = SDL_SCANCODE_KP_4;
        break;
    case Numpad5:
        check_key = SDL_SCANCODE_KP_5;
        break;
    case Numpad6:
        check_key = SDL_SCANCODE_KP_6;
        break;
    case Numpad7:
        check_key = SDL_SCANCODE_KP_7;
        break;
    case Numpad8:
        check_key = SDL_SCANCODE_KP_8;
        break;
    case Numpad9:
        check_key = SDL_SCANCODE_KP_9;
        break;
    case F1:
        check_key = SDL_SCANCODE_F1;
        break;
    case F2:
        check_key = SDL_SCANCODE_F2;
        break;
    case F3:
        check_key = SDL_SCANCODE_F3;
        break;
    case F4:
        check_key = SDL_SCANCODE_F4;
        break;
    case F5:
        check_key = SDL_SCANCODE_F5;
        break;
    case F6:
        check_key = SDL_SCANCODE_F6;
        break;
    case F7:
        check_key = SDL_SCANCODE_F7;
        break;
    case F8:
        check_key = SDL_SCANCODE_F8;
        break;
    case F9:
        check_key = SDL_SCANCODE_F9;
        break;
    case F10:
        check_key = SDL_SCANCODE_F10;
        break;
    case F11:
        check_key = SDL_SCANCODE_F11;
        break;
    case F12:
        check_key = SDL_SCANCODE_F12;
        break;
    case F13:
        check_key = SDL_SCANCODE_F13;
        break;
    case F14:
        check_key = SDL_SCANCODE_F14;
        break;
    case F15:
        check_key = SDL_SCANCODE_F15;
        break;
    case Pause:
        check_key = SDL_SCANCODE_PAUSE;
        break;
    }
    if (keyboard_state[check_key])
    {
        return true;
    }
    return false;
}

} // namespace Keyboard

} // namespace BlueSnow
