#define STB_IMAGE_IMPLEMENTATION

#include <fstream>
#include <iostream>

#include <SDL2/SDL.h>
#include <json.hpp>
#include <stb_image.h>

#include "core/Exceptions.hpp"
#include "core/GlobalSettings.hpp"
#include "core/Logger.hpp"
#include "core/init.hpp"

#include "Tracy.hpp"

namespace BlueSnow
{
void GLAPIENTRY MessageCallback(GLenum source, GLenum type, GLuint id,
                                GLenum severity, GLsizei length,
                                const GLchar *message, const void *userParam)
{
    fprintf(stderr,
            "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""), type,
            severity, message);
}

void init()
{
    ZoneScopedN("Init");

    std::ifstream in_settings_file("BlueSnow.json");

    nlohmann::json settings_file;

    in_settings_file >> settings_file;
    in_settings_file.close();

    GlobalSettings *settings = &GlobalSettings::get();

    settings->name = settings_file["name"];
    settings->working_dir = settings_file["working_dir"];

    char *perf = SDL_GetPrefPath("BlueSnow", settings->name.c_str());
    settings->settings_dir = std::string(perf);
    SDL_free(perf);

    LuaState::get().init();
}

void InitGL()
{
    ZoneScopedN("Init GL");
    if (!gladLoadGL((GLADloadfunc)SDL_GL_GetProcAddress))
    {
        LOG(ERROR) << "Failed to init OpenGL";
        exit(-1);
    }

    LOG(DEBUG) << "GL Vendor: " << glGetString(GL_VENDOR);
    LOG(DEBUG) << "GL Renderer: " << glGetString(GL_RENDERER);
    LOG(DEBUG) << "GL Version: " << glGetString(GL_VERSION);
    LOG(DEBUG) << "GL Shading Language: "
              << glGetString(GL_SHADING_LANGUAGE_VERSION);

    if (!GLAD_GL_ARB_explicit_uniform_location || !GLAD_GL_VERSION_4_4)
    {
        LOG(ERROR) << "ARB_explicit_uniform_location not supported!";
        exit(-1);
    }

    glEnable(GL_DEPTH_TEST);
#ifndef FINAL_BUILD
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(MessageCallback, 0);
#endif
}
} // namespace BlueSnow
