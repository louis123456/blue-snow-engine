#pragma once

#include <glm/glm.hpp>

namespace glm
{
typedef vec<2, int> vec2i;

} // namespace glm
