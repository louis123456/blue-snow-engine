#pragma once

#include <cstdint>
#include <exception>
#include <string>
#include <vector>

namespace BlueSnow
{
enum BinaryDataEncoding
{
    RAW,
    BASE64,
};

class BinaryBuffer
{
private:
    std::vector<unsigned char> m_data;

    /**
     * \brief Encode Base64
     *
     * \param input_buffer
     * \return const std::string
     */
    std::string base64_encode(std::vector<unsigned char> input_buffer);

    /**
     * \brief Decode base64
     *
     * \param input
     * \return const std::vector<unsigned char>
     */
    std::vector<unsigned char> base64_decode(const std::string &input);

    /**
     * \brief Create a vector from a string
     *
     * \param input
     * \return std::vector<unsigned char>
     */
    std::vector<unsigned char> to_vector(const std::string &input);

public:
    /**
     * \brief Constructs a new Binary Buffer
     *
     */
    BinaryBuffer();

    /**
     * \brief Constructs a new Binary Buffer with Base64 data
     *
     * \param data The data.
     */
    BinaryBuffer(const std::string &data);

    /**
     * \brief Constructs a new Binary Buffer with Raw data
     *
     * \param data
     */
    BinaryBuffer(const std::vector<unsigned char> &data);

    /**
     * \brief Sets the data Base64
     *
     * \param data
     */
    void set_data(const std::string &data);

    /**
     * \brief Sets the data raw
     *
     * \param data
     */

    void set_data(const std::vector<unsigned char> &data);

    /**
     * \brief Overloads the [] operator, used to get a byte at index pos.
     *
     * \param index The index to get
     * \return char& Ihe data stored at index
     */
    unsigned char &operator[](int index) { return m_data[index]; }
};
} // namespace BlueSnow
