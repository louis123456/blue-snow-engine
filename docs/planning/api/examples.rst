Examples to determine the feel of the API
=========================================

Implementation will be with GLFW to make
my life easier.

Creation of window with GLContext
---------------------------------

.. code:: cpp

    #include <BlueSnowEngine.hpp>
    //...

    // Create settings
    BlueSnow::WindowSettings settings;
    settings.resolution = {800, 600};
    settings.title = "Hello world!!";

    // Make a window with the setings;
    BlueSnow::RenderWindow win(settings);
    /////////////OR////////////////
    BlueSnow::RenderWindow win;
    win.create(settings);

Creation and rendering of scene
-------------------------------

.. code:: cpp

    #include <BlueSnowEngine.hpp>
    //...

    // Create Scene
    BlueSnow::Scene scene;

    // Add object to scene
    scene.add(object);

    // Create a light
    BlueSnow::Light

    // Add Light to scene
    scene.add(light);

    int cam = scene.add_camera(camera);
    scene.set_active_camera(cam);

    scene.draw();

Loading of mesh
---------------
.. code:: cpp

    #include <BlueSnowEngine.hpp>
    //...

    // Create Mesh
    BlueSnow::ComponentMesh mesh;
    mesh.from_file("mesh.gltf");

Creation of a Shader and applying to object
-------------------------------------------

.. code:: cpp

    #include <BlueSnowEngine.hpp>
    //...

    // Create Shader
    BlueSnow::PBRShader shader;

    // Load texture
    BlueSnow::Texture diffuse;
    diffuse.load_file("diffuse.png");

    // Load Normals
    BlueSnow::Texture normal;
    normal.load_file("normal.png");

    // Create Material
    BlueSnow::Material mat(shader);

    // mat.set_colour(BlueSnow::ColourFloat(1.f, 0.f, 0.f, 1.f));
    mat.set_texture0(diffuse);
    mat.set_texture1(normal);

    mesh.set_material(mat);

    // Apply to object
    BlueSnow::Object object;
    object.add_component(mesh);


