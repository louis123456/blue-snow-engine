#include <iostream>

#include "io/BinaryBuffer.hpp"

namespace BlueSnow
{
BinaryBuffer::BinaryBuffer() {}

////////////////////////////////////////////////////////////////////////////////

BinaryBuffer::BinaryBuffer(const std::string &data) { set_data(data); }

////////////////////////////////////////////////////////////////////////////////

BinaryBuffer::BinaryBuffer(const std::vector<unsigned char> &data)
{
    set_data(data);
}

////////////////////////////////////////////////////////////////////////////////

std::vector<unsigned char> BinaryBuffer::to_vector(const std::string &input)
{
    std::vector<unsigned char> out;
    for (char inchar : input)
    {
        out.push_back(inchar);
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

void BinaryBuffer::set_data(const std::string &data)
{
    m_data = base64_decode(data);
}

////////////////////////////////////////////////////////////////////////////////

void BinaryBuffer::set_data(const std::vector<unsigned char> &data)
{
    m_data = data;
}

////////////////////////////////////////////////////////////////////////////////

std::string BinaryBuffer::base64_encode(std::vector<unsigned char> input_buffer)
{
    const char encodeLookup[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    const char padCharacter = '=';
    std::string encodedString;
    encodedString.reserve(
        ((input_buffer.size() / 3) + (input_buffer.size() % 3 > 0)) * 4);
    uint32_t temp;
    std::vector<unsigned char>::iterator cursor = input_buffer.begin();
    for (size_t idx = 0; idx < input_buffer.size() / 3; idx++)
    {
        temp = (*cursor++) << 16;
        temp += (*cursor++) << 8;
        temp += (*cursor++);
        encodedString.append(1, encodeLookup[(temp & 0x00FC0000) >> 18]);
        encodedString.append(1, encodeLookup[(temp & 0x0003F000) >> 12]);
        encodedString.append(1, encodeLookup[(temp & 0x00000FC0) >> 6]);
        encodedString.append(1, encodeLookup[(temp & 0x0000003F)]);
    }
    switch (input_buffer.size() % 3)
    {
    case 1:
        temp = (*cursor++) << 16;
        encodedString.append(1, encodeLookup[(temp & 0x00FC0000) >> 18]);
        encodedString.append(1, encodeLookup[(temp & 0x0003F000) >> 12]);
        encodedString.append(2, padCharacter);
        break;
    case 2:
        temp = (*cursor++) << 16;
        temp += (*cursor++) << 8;
        encodedString.append(1, encodeLookup[(temp & 0x00FC0000) >> 18]);
        encodedString.append(1, encodeLookup[(temp & 0x0003F000) >> 12]);
        encodedString.append(1, encodeLookup[(temp & 0x00000FC0) >> 6]);
        encodedString.append(1, padCharacter);
        break;
    }
    return encodedString;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<unsigned char> BinaryBuffer::base64_decode(const std::string &input)
{
    const char encodeLookup[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    const char padCharacter = '=';
    size_t padding = 0;
    if (input.length())
    {
        if (input[input.length() - 1] == padCharacter)
            padding++;
        if (input[input.length() - 2] == padCharacter)
            padding++;
    }
    // Setup a vector to hold the result
    std::vector<unsigned char> decodedBytes;
    decodedBytes.reserve(((input.length() / 4) * 3) - padding);
    uint32_t temp = 0; // Holds decoded quanta
    std::string::const_iterator cursor = input.begin();
    while (cursor < input.end())
    {
        for (size_t quantumPosition = 0; quantumPosition < 4; quantumPosition++)
        {
            temp <<= 6;
            if (*cursor >= 0x41 &&
                *cursor <= 0x5A)        // This area will need tweaking if
                temp |= *cursor - 0x41; // you are using an alternate alphabet
            else if (*cursor >= 0x61 && *cursor <= 0x7A)
                temp |= *cursor - 0x47;
            else if (*cursor >= 0x30 && *cursor <= 0x39)
                temp |= *cursor + 0x04;
            else if (*cursor == 0x2B)
                temp |= 0x3E; // change to 0x2D for URL alphabet
            else if (*cursor == 0x2F)
                temp |= 0x3F;                 // change to 0x5F for URL alphabet
            else if (*cursor == padCharacter) // pad
            {
                switch (input.end() - cursor)
                {
                case 1: // One pad character
                    decodedBytes.push_back((temp >> 16) & 0x000000FF);
                    decodedBytes.push_back((temp >> 8) & 0x000000FF);
                    return decodedBytes;
                case 2: // Two pad characters
                    decodedBytes.push_back((temp >> 10) & 0x000000FF);
                    return decodedBytes;
                }
            }
            cursor++;
        }
        decodedBytes.push_back((temp >> 16) & 0x000000FF);
        decodedBytes.push_back((temp >> 8) & 0x000000FF);
        decodedBytes.push_back((temp)&0x000000FF);
    }
    return decodedBytes;
}
} // namespace BlueSnow
