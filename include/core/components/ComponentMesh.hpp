#pragma once

#include <cstdint>
#include <vector>

#include <glm/glm.hpp>

#include "core/components/Component.hpp"
#include "io/BufferView.hpp"

namespace BlueSnow
{
class ComponentMesh : public Component, public Transform
{
private:
    std::vector<glm::vec3> m_vertices; ///< Vertices of the Mesh
    std::vector<glm::vec3> m_normals;  ///< Normals of the Mesh
    std::vector<glm::vec2> m_uvs;      ///< UV'S of the mesh
    std::vector<uint16_t> m_indices;   ///< The mesh indacies

    bool m_mesh_static = true;
    bool m_draw_mesh = true;

    uint m_VBO = 0;
    uint m_NVBO = 0;
    uint m_UVBO = 0;
    uint m_VAO = 0;
    uint m_EBO = 0;

    void generate_gl_buffers();
    void update_gl_vertex_buffer();
    void update_gl_uv_buffer();

    bool m_loaded = false;

public:
    /**
     * \brief Constructs a new Mesh
     *
     */
    ComponentMesh();

    /**
     * \brief Constructs a new Mesh with data
     *
     * \param data json metadata
     * \param vertices
     * \param normals
     * \param uvs
     * \param indicies
     */
    ComponentMesh(const std::string &data, BufferView vertices,
                  BufferView normals, BufferView uvs, BufferView indicies);

    /**
     * \brief Constructs a new Mesh with the data.
     *
     * \param data The json string to load from.
     */
    explicit ComponentMesh(const std::string &data);

    /**
     * \brief Clones the Component
     *
     * \return
     */
    ComponentMesh *clone() const { return new ComponentMesh(*this); }

    /**
     * \brief Get the type
     *
     * \return std::string
     */
    std::string get_type() override { return "Mesh"; }

    /**
     * \brief Loads all data from json passed into data
     *
     * \param data The json string to load from
     */
    void load(const std::string &data) override;

    /**
     * \brief Unload all data
     *
     */
    void unload() override;
    /**
     * \brief Called on startup
     *
     */
    void setup() override;

    /**
     * \brief Called before every frame.
     *
     * \param delta_time Time passed since last call
     */
    void update(std::chrono::microseconds delta_time) override;

    /**
     * \brief Draw the frame.
     *
     */
    void frame() const override;

    /**
     * \brief Called after each frame.
     *
     * \param delta_time
     */
    void post_update(std::chrono::microseconds delta_time) override;

    /**
     * \brief Creates a json string with Object data.
     *
     */
    std::string save() const override;

    /**
     * \brief Called at a constant rate.
     *
     */
    void tick() override;

    /**
     * \brief Destroy the Mesh
     *
     */
    ~ComponentMesh();
};
} // namespace BlueSnow
