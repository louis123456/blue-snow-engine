#pragma once

#include <iostream>

#include <glad/glad.h>

#include <SDL2/SDL.h>

#include <GL/glx.h>

#include "core/events/Event.hpp"
#include "window/IWindow.hpp"

namespace BlueSnow
{
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display *, GLXFBConfig,
                                                     GLXContext, Bool,
                                                     const int *);
/**
 * \brief X11 version of the Render Window
 *
 */
class RenderWindow : public IWindow
{
private:
    SDL_Window *m_window;

    SDL_GLContext m_gl_context;

public:
    /**
     * \brief Construct a new Window object, does not create the window.
     *
     */
    RenderWindow();

    /**
     * \brief Construct from a WindowSettings struct and create a Window.
     *
     * The WindowSettings struct contains all the required information for the
     * creation of the Window.
     *
     * \param settings The Settings used in the creation of the Window.
     */
    explicit RenderWindow(const WindowSettings &settings);

    /**
     * \brief Creates the Window, called by the Constructor aswell.
     *
     * \param settings he Settings used in the creation of the Window.
     */
    void create(const WindowSettings &settings);

    /**
     * \brief Checks For and returns events
     *
     * \return Event
     */
    Event poll_events();

    /**
     * \brief Close the window
     *
     */
    void close_window();

    /**
     * \brief Swap the buffers of the Window
     *
     */
    void swap_buffers();

    /**
     * \brief Destroy the Window object.
     *
     */
    ~RenderWindow();
};
} // namespace BlueSnow
