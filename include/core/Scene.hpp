#pragma once

#include <chrono>
#include <memory>
#include <vector>

#include <glad/glad.h>

#include "core/Object.hpp"
#include "physics/PhysicsWorld.hpp"

namespace BlueSnow
{
class SceneManager;
class Scene
{
private:
    std::vector<std::shared_ptr<Object>> m_objects; ///< The Loaded Objects

    std::vector<std::shared_ptr<Object>>
        m_instances; ///< The instances of objects in the scene

    SceneManager *m_parent;

    void load_object_file(const std::string &file);

    int get_object_index_by_name(const std::string &name);
    int get_instance_index_by_name(const std::string &name);

    std::string m_name;

    bool m_setup = false;

    PhysicsWorld m_physics_world;

public:
    /**
     * \brief Construct a new Scene object
     *
     */
    Scene();

    PhysicsWorld &get_physics_world()
    {
        return std::ref<PhysicsWorld>(m_physics_world);
    }

    /**
     * \brief Set the parent of the scene
     *
     * \param parent
     */
    virtual void set_parent(SceneManager *parent) { m_parent = parent; }

    /**
     * \brief Get the parent object of the scene
     *
     * \return SceneManager* a pointer to the scenes manager
     */
    virtual SceneManager *get_parent() { return m_parent; }

    /**
     * \brief Sets the name
     *
     * \param name
     */
    void set_name(const std::string &name) { m_name = name; }

    /**
     * \brief Gets an object by name
     *
     * \return Object
     */
    std::shared_ptr<Object> get_object_by_name(const std::string &name);

    /**
     * \brief Gets an object instance by name
     *
     * \return Object
     */
    std::shared_ptr<Object> get_instance_by_name(const std::string &name);

    /**
     * \brief Gets the name
     *
     * \return const std::string&
     */
    const std::string &get_name() const { return m_name; }

    /**
     * \brief Loads from json file
     *
     * \param path The file to load from
     */
    void load(const std::string &path);

    /**
     * \brief Unload all data
     *
     */
    void unload();

    /**
     * \brief Set the scene and opengl up
     *
     */
    void setup();

    /**
     * \brief Returns true when \ref setup() did run
     *
     */
    bool was_setup() { return m_setup; }

    /**
     * \brief Called before every frame.
     *
     * \param delta_time Time passed since last call
     */
    void update(std::chrono::microseconds delta_time);

    /**
     * \brief Draw the frame.
     *
     */
    void frame() const;

    /**
     * \brief Called after each frame.
     *
     * \param delta_time Time passed since last call
     */
    void post_update(std::chrono::microseconds delta_time);

    /**
     * \brief Called at a constant rate
     *
     */
    void tick(float tickrate);

    /**
     * \brief Save the string data
     *
     */
    void save() const;

    /**
     * \brief Destroy the Scene object
     *
     */
    ~Scene();
};
} // namespace BlueSnow
