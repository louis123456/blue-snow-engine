#pragma once

#include <string>

#include <glad/glad.h>

namespace BlueSnow
{
class Shader
{
protected:
    int m_vertex_shader = -1; ///< The vertex shader.
    int m_fragment_shader = -1; ///< The fragment shader.
    int m_shader_program = -1; ///< the Linked shader.

    std::string m_vertex_source; ///< The vertex shader source.
    std::string m_fragment_source; ///< The fragment shader source.

public:
    /**
     * \brief Construct a new Shader object
     *
     */
    Shader();

    /**
     * \brief Construct a new Shader object with the source. Does not compile.
     * 
     * \param vertex The vertex shader source.
     * \param fragment The fragment shader source.
     */
    Shader(const std::string &vertex, const std::string &fragment);

    /**
     * \brief Load the shader sources from a file.
     * 
     * \param vertex The vertex source file
     * \param fragment The fragment source file
     * \return true Sucess
     * \return false Failure
     */
    bool load_file(const std::string &vertex, const std::string &fragment);

    /**
     * \brief Compiles and links the program
     * 
     * \return true Sucess
     * \return false Failure
     */
    bool compile_and_link();

    /**
     * \brief Compiles the shader program
     *
     * \return true Sucess
     * \return false Failure
     */
    bool compile();

    /**
     * \brief Get the shader ID
     *
     * \return int The GL shader.
     */
    int get_shader() const;

    /**
     * \brief links the shader program
     *
     * \return true Sucess
     * \return false Failure
     */
    bool link();

    /**
     * \brief Use the shader program for the next drawcalls.
     *
     */
    void use();

    /**
     * \brief Destroy the Shader object
     *
     */
    ~Shader();
};
} // namespace BlueSnow
