#pragma once

#include <string>

#include <glad/glad.h>
#include <stb_image.h>

#include "core/Logger.hpp"
#include "render/TextureSettings.hpp"

namespace BlueSnow
{
class Texture
{
private:
    unsigned int m_texture_id;

public:
    /**
     * \brief Constructs a Texture
     *
     */
    Texture();

    /**
     * \brief Constructs a texture with file.
     *
     * \param path Path to le texture.
     */
    Texture(const std::string &path,
            const TextureSettings &settings = TextureSettings());

    /**
     * \brief Load the texture.
     *
     */
    void load(const std::string &path, const TextureSettings &settings);

    /**
     * \brief Bind the texture in a GL texture slot
     *
     * \param slot The slot to bind it in.
     */
    void bind(int slot);

    /**
     * \brief Unload the texture;
     *
     */
    void unload();

    /**
     * \brief Destroy the Texture
     *
     */
    ~Texture() { unload(); }
};
} // namespace BlueSnow
