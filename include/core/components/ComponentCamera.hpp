#pragma once

#include <chrono>
#include <string>

#include <glm/glm.hpp>

#include "core/Transform.hpp"
#include "core/components/Component.hpp"

namespace BlueSnow
{
class ComponentCamera : public Component, public Transform
{
private:
    float m_fov;
    float m_clip_near;
    float m_clip_far;

public:
    /**
     * \brief Constructs a Camera
     *
     */
    ComponentCamera();

    /**
     * \brief Constructs a Camera from data
     *
     * \param data
     */
    explicit ComponentCamera(const std::string &data) { load(data); }

    /**
     * \brief Clones the Component
     *
     * \return
     */
    ComponentCamera *clone() const { return new ComponentCamera(*this); }

    /**
     * \brief Get the type
     *
     * \return std::string
     */
    std::string get_type() { return "Camera"; }

    /**
     * \brief Loads all data from json passed into data
     *
     * \param data The json string to load from
     */
    void load(const std::string &data);

    /**
     * \brief Unload all data
     *
     */
    void unload();
    /**
     * \brief Called on startup
     *
     */
    void setup();

    /**
     * \brief Called before every frame.
     *
     * \param delta_time Time passed since last call
     */
    void update(std::chrono::microseconds delta_time);

    /**
     * \brief Draw the frame.
     *
     */
    void frame() const;

    /**
     * \brief Called after each frame.
     *
     * \param delta_time
     */
    void post_update(std::chrono::microseconds delta_time);

    /**
     * \brief Creates a json string with Object data.
     *
     */
    std::string save() const;

    /**
     * \brief Called at a constant rate.
     *
     */
    void tick();

    ~ComponentCamera();
};
} // namespace BlueSnow
