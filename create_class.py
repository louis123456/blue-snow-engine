#!/usr/bin/python

import os
import sys
import io

if len(sys.argv) < 3:
    exit(0);

class_section = sys.argv[1]
class_name = sys.argv[2]

if len(sys.argv) >= 4:
    class_template = sys.argv[3]
else:
    class_template = "class"

if len(sys.argv) >= 5:
    class_prefix = sys.argv[4]
else:
    class_prefix = ""

file_template_hpp = io.open("class_template/" + class_template + ".hpp.template", "r")
file_template_cpp = io.open("class_template/" + class_template + ".cpp.template", "r")

file_out_hpp = io.open("include/" + class_section + "/" + class_prefix + class_name + ".hpp", "w+")
file_out_cpp = io.open("src/" + class_section + "/" + class_prefix + class_name + ".cpp", "w+")

for line in file_template_hpp:
    file_out_hpp.write(line.replace("{class_name}", class_name))

file_template_hpp.close()
file_out_hpp.close()

for line in file_template_cpp:
    file_out_cpp.write(line.replace("{class_name}", class_name).replace("{include_file}", class_section + "/" + class_name + ".hpp"))
    
file_template_cpp.close()
file_out_cpp.close()