Resources and research
======================

Core
----

https://en.wikipedia.org/wiki/Game_engine

Events
------

https://bastian.rieck.ru/blog/posts/2015/event_system_cxx11/

Renderer
--------

Physics
-------

https://en.wikipedia.org/wiki/Bullet_(software)
https://pybullet.org/wordpress/
https://github.com/bulletphysics/bullet3/tree/master/docs
https://pybullet.org/Bullet/BulletFull/

Audio
-----

https://xiph.org/vorbis/ https://en.wikipedia.org/wiki/Vorbis
https://www.openal.org/documentation/
https://www.gamedev.net/articles/programming/general-and-gameplay-programming/introduction-to-ogg-vorbis-r2031/

Models
------

https://en.wikipedia.org/wiki/FBX

AI
--

https://www.oreilly.com/library/view/ai-for-game/0596005555/ch01.html
https://en.wikipedia.org/wiki/Artificial_intelligence_in_video_games
