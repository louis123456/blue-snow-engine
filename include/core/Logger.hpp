// Adapted from https://stackoverflow.com/questions/5028302/small-logger-class

#pragma once

#ifndef LOG_H
#define LOG_H

#include <iostream>
#include <string>

#define LOG(level) Logger(__FILE__, __LINE__, level)

#ifdef __linux__
#define COLOUR_RESET std::string("\e[0m")
#define COLOUR_DEBUG std::string("\e[97m")
#define COLOUR_INFO std::string("\e[32m")
#define COLOUR_WARN std::string("\e[33m")
#define COLOUR_ERROR std::string("\e[31m")

#else
#define COLOUR_RESET std::string("")
#define COLOUR_DEBUG std::string("")
#define COLOUR_INFO std::string("")
#define COLOUR_WARN std::string("")
#define COLOUR_ERROR std::string("")

#endif

#define DEBUG 0
#define INFO 1
#define WARN 2
#define ERROR 3

static inline char *timenow()
{
    static char buffer[64];
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, 64, "[%Y-%m-%d %H:%M:%S]", timeinfo);

    return buffer;
}

struct structlog
{
    bool headers = true;
    int level = DEBUG;
    bool location = false;
};

extern structlog LOGCFG;

class Logger
{
public:
    Logger() {}
    explicit Logger(const std::string &location, int line, int type)
    {
        msglevel = type;
        if (LOGCFG.headers)
        {
            operator<<(getLabel(location, line, type));
        }
    }
    ~Logger()
    {
        if (opened)
        {
            std::cout << COLOUR_RESET << std::endl;
        }
        opened = false;
    }
    template <class T>
    Logger &operator<<(const T &msg)
    {
        if (msglevel >= LOGCFG.level)
        {
            std::cout << msg;
            opened = true;
        }
        return *this;
    }

private:
    bool opened = false;
    int msglevel = DEBUG;
    inline std::string getLabel(const std::string &location, int line, int type)
    {
        std::string label = "";
        switch (type)
        {
        case DEBUG:
            label += COLOUR_DEBUG + timenow() + "[DEBUG]";
            break;
        case INFO:
            label += COLOUR_INFO + timenow() + "[ INFO]";
            break;
        case WARN:
            label += COLOUR_WARN + timenow() + "[ WARN]";
            break;
        case ERROR:
            label += COLOUR_ERROR + timenow() + "[ERROR]";
            break;
        }
        if (LOGCFG.location)
            label += "[" + location + ":" + std::to_string(line) + "]";
        label += " ";
        return label;
    }
};

inline void logger_debug(const std::string &log_text)
{
    LOG(DEBUG) << log_text;
}

inline void logger_info(const std::string &log_text) { LOG(INFO) << log_text; }

inline void logger_warn(const std::string &log_text) { LOG(WARN) << log_text; }

inline void logger_error(const std::string &log_text)
{
    LOG(ERROR) << log_text;
}

#endif /* LOG_H */
