import matplotlib.pyplot as plt 
import os
import io
import numpy as np
import re

def getListOfFiles(dirName):
    # create a list of file and sub directories 
    # names in the given directory 
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        elif fullPath == "../include/glad/glad.h":
            continue
        elif fullPath == "../include/KHR/khrplatform.h":
            continue
        elif fullPath == "../src/glad.c":
            continue
        else:
            allFiles.append(fullPath)
                
    return allFiles

def count_files(paths):
    cpp = 0
    comment = 0
    blank = 0
    total = 0

    largest_file = 0
    largest_file_string = ""

    for path in paths:
        counts = count_file(path)
        if counts[3] > largest_file:
            largest_file = counts[3]
            largest_file_string = path
        cpp += counts[0]
        comment += counts[1]
        blank += counts[2]
        total += counts[3]

    return ([('C++', cpp), ('Comments', comment), ('Blank', blank), ('Total',total)], [largest_file_string, largest_file])

def count_file(path):
    file = io.open(path, "r")
    inline_block_comment = "/\*\*/"
    block_comment_start = ".*/\*.*"
    block_comment_end = ".*\*/.*"
    comment_line = "\s*//.*"
    shared_line = ".*//.*"
    blank_line = "^\s*$"

    cpp = 0
    comment = 0
    blank = 0
    total = 0
    in_comment = False;
    line = file.readline()
    while line:
        total += 1
        if re.match(inline_block_comment, line):
            comment += 1
        if re.match(block_comment_start, line):
            in_comment = True
            comment += 1
        elif re.match(block_comment_end, line):
            in_comment = False
            comment += 1
        elif in_comment:
            comment += 1
        if re.match(comment_line, line):
            comment += 1
        if re.match(shared_line, line):
            cpp += 1
            comment += 1
        if re.match(blank_line, line):
            blank += 1
        else:
            cpp += 1
        line = file.readline()
    file.close()

    return [cpp, comment, blank, total]

# heights of bars
counts = count_files(getListOfFiles("../include") + getListOfFiles("../src"))

loc_file = io.open("loc.rst", "w")

loc_file.write("Total LOC: " + str(counts[0][3][1]) + "\n\n");
loc_file.write("Largest File:  " + str(counts[1][0]) + " with " + str(counts[1][1]) + " LOC\n\n");

loc_file.close();