#include "core/LuaCore.hpp"
#include "scripting/LuaState.hpp"

namespace BlueSnow
{
void LuaState::init()
{
    m_luastate.open_libraries(sol::lib::base, sol::lib::package);

    register_types();
}

LuaState &LuaState::get()
{
    static LuaState inst;
    return inst;
}

void LuaState::register_types() { register_core_lua(m_luastate); }

sol::environment LuaState::create_new_sandbox()
{
    sol::environment env(m_luastate, sol::create);

    env["log_debug"] = &logger_debug;
    env["log_info"] = &logger_info;
    env["log_warn"] = &logger_warn;
    env["log_error"] = &logger_error;

    env["Object"] = m_luastate["Object"];
    env["Scene"] = m_luastate["Scene"];
    env["vec3"] = m_luastate["vec3"];
    env["ComponentTransform"] = m_luastate["ComponentTransform"];

    return env;
}

} // namespace BlueSnow