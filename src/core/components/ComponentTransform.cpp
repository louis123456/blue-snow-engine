#include "core/RenderMatrices.hpp"
#include "core/components/ComponentTransform.hpp"

namespace BlueSnow
{

ComponentTransform::ComponentTransform() {}

////////////////////////////////////////////////////////////////////////////////

void ComponentTransform::load(const std::string &data)
{
    nlohmann::json json_data = nlohmann::json::parse(data);

    m_name = "Transform";
    translate(glm::vec3(json_data["position"][0], //
                        json_data["position"][1], //
                        json_data["position"][2]  //
                        ));

    scale(glm::vec3(json_data["scale"][0], //
                    json_data["scale"][1], //
                    json_data["scale"][2]  //
                    ));

    rotate(glm::vec3(json_data["rotation"][0], //
                     json_data["rotation"][1], //
                     json_data["rotation"][2]  //
                     ));
}

////////////////////////////////////////////////////////////////////////////////

void ComponentTransform::ComponentTransform::unload() {}

////////////////////////////////////////////////////////////////////////////////

void ComponentTransform::setup() {}

////////////////////////////////////////////////////////////////////////////////

void ComponentTransform::update(std::chrono::microseconds delta_time) {}

////////////////////////////////////////////////////////////////////////////////

void ComponentTransform::frame() const
{
    RenderMatrices::get().Object = m_transform;
}

////////////////////////////////////////////////////////////////////////////////

void ComponentTransform::post_update(std::chrono::microseconds delta_time) {}

////////////////////////////////////////////////////////////////////////////////

std::string ComponentTransform::save() const {}

////////////////////////////////////////////////////////////////////////////////

void ComponentTransform::tick() {}

////////////////////////////////////////////////////////////////////////////////

ComponentTransform::~ComponentTransform() {}

} // namespace BlueSnow
