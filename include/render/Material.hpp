#pragma once

#include <array>

#include <glm/glm.hpp>
#include <json.hpp>

#include "render/Shader.hpp"
#include "render/Texture.hpp"

namespace BlueSnow
{
enum VariableTypes
{
    UNIFORM_TEXTURE = 0,
    UNIFORM_INT = 1,
    UNIFORM_UINT = 2,
    UNIFORM_FLOAT = 3,
    UNIFORM_DOUBLE = 4,
    UNIFORM_VEC2 = 5,
    UNIFORM_VEC3 = 6,
    UNIFORM_VEC4 = 7,
    UNIFORM_MAT3 = 8,
    UNIFORM_MAT4 = 9
};

class Material
{
private:
    std::vector<Texture *> m_textures;

    std::vector<std::pair<int, int>> m_int_uniforms;
    std::vector<std::pair<int, uint>> m_uint_uniforms;
    std::vector<std::pair<int, float>> m_float_uniforms;
    std::vector<std::pair<int, double>> m_double_uniforms;
    std::vector<std::pair<int, glm::vec2>> m_vec2_uniforms;
    std::vector<std::pair<int, glm::vec3>> m_vec3_uniforms;
    std::vector<std::pair<int, glm::vec4>> m_vec4_uniforms;
    std::vector<std::pair<int, glm::mat3>> m_mat3_uniforms;
    std::vector<std::pair<int, glm::mat4>> m_mat4_uniforms;

    Shader m_shader;

    std::string m_name;

public:
    /**
     * \brief Constructs a Material
     *
     */
    Material();

    /**
     * \brief Constructs a Material with json data
     *
     * \param data jsnon metadata
     */
    explicit Material(const std::string &data) { load(data); }

    /**
     * \brief Sets the name
     *
     * \param name
     */
    void set_name(const std::string &name) { m_name = name; }

    /**
     * \brief Gets the name
     *
     * \return const std::string&
     */
    const std::string &get_name() const { return m_name; }

    /**
     * \brief Load the material from json string.
     *
     * \param data
     */
    void load(const std::string &data);

    /**
     * \brief Use the Material
     *
     */
    void use();

    /**
     * \brief Unload the material
     *
     */
    void unload();

    /**
     * \brief Destroys the Material
     *
     */
    ~Material();
};
} // namespace BlueSnow
