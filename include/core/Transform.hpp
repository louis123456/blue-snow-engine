#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/matrix_decompose.hpp>

namespace BlueSnow
{
class Transform
{
private:
protected:
    glm::mat4 m_transform =
        glm::mat4(1.0f); ///< The Transform matrix of the object.
public:
    /**
     * \brief Gets the transform
     *
     * \return glm::mat4 The transform
     */
    glm::mat4 get_transform() const;

    /**
     * \brief Sets the transform.
     *
     * \param transform The transform to set for
     */
    void set_transform(glm::mat4 transform);

    /**
     * \brief Sets the position.
     *
     * \param pos The position
     */
    void set_position(glm::vec3 pos);

    /**
     * \brief Sets the rotation.
     *
     * \param rot The rotation
     */
    void set_rotation(glm::vec3 rot);

    /**
     * \brief Sets the rotation.
     *
     * \param rot The rotation
     */
    void set_rotation(glm::quat rot);

    /**
     * \brief Sets the scale
     *
     * \param scale_in The scale
     */
    void set_scale(glm::vec3 scale_in);

    /**
     * \brief Translates the object
     *
     * \param pos The vector to translate with
     */
    void translate(glm::vec3 pos);

    /**
     * \brief Rotates the object
     *
     * \param rot The vector to translate with
     */
    void rotate(glm::vec3 rot);

    /**
     * \brief Rotates the object
     *
     * \param rot Quat to roate by
     */
    void rotate(glm::quat rot);

    /**
     * \brief Scales the object.
     *
     * \param scale_in
     */
    void scale(glm::vec3 scale_in);

    /**
     * \brief Gets the position
     *
     * \return glm::vec3 The current position
     */
    glm::vec3 get_position() const;

    /**
     * \brief Gets the rotation
     *
     * \return glm::vec3
     */
    glm::vec3 get_rotation() const;

    /**
     * \brief Gets the rotation
     *
     * \return glm::quat
     */
    glm::quat get_rotation_quaternion() const;

    /**
     * \brief Gets the scale
     *
     * \return glm::vec3
     */
    glm::vec3 get_scale() const;
};
} // namespace BlueSnow
