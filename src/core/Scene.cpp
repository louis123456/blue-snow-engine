#include <fstream>
#include <iostream>

#include <chrono>
#include <glm/glm.hpp>
#include <json.hpp>

#include "core/GlobalSettings.hpp"
#include "core/Logger.hpp"
#include "core/RenderMatrices.hpp"
#include "core/Scene.hpp"

#include "Tracy.hpp"

namespace BlueSnow
{
Scene::Scene() : m_physics_world() {}

////////////////////////////////////////////////////////////////////////////////

int Scene::get_object_index_by_name(const std::string &name)
{
    for (int i = 0; i < m_objects.size(); i++)
    {
        if (m_objects[i]->get_name() == name)
            return i;
    }

    return -1;
}

////////////////////////////////////////////////////////////////////////////////

int Scene::get_instance_index_by_name(const std::string &name)
{
    for (int i = 0; i < m_instances.size(); i++)
    {
        if (m_instances[i]->get_name() == name)
            return i;
    }

    return -1;
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<Object> Scene::get_object_by_name(const std::string &name)
{
    int index = get_object_index_by_name(name);
    if (index < 0)
    {
        return nullptr;
    }
    return m_objects[index];
}

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<Object> Scene::get_instance_by_name(const std::string &name)
{
    int index = get_instance_index_by_name(name);
    if (index < 0)
    {
        return nullptr;
    }
    return m_instances[index];
}

////////////////////////////////////////////////////////////////////////////////

void Scene::load_object_file(const std::string &file)
{
    std::shared_ptr<Object> obj(new Object(file));

    obj->set_parent(this);
    m_objects.push_back(obj);
}

////////////////////////////////////////////////////////////////////////////////

void Scene::load(const std::string &path)
{
    ZoneScopedN("Loading scene");
    auto time_begin = std::chrono::high_resolution_clock::now();

    std::ifstream file(GlobalSettings::get().working_dir + path);
    nlohmann::json json_data;
    file >> json_data;
    file.close();

    m_name = json_data["scene"]["name"];

    LOG(INFO) << "Loading scene " << m_name << ".";

    LOG(INFO) << "Loading Objects";
    for (std::string path : json_data["load"])
    {
        load_object_file(path);
    }

    LOG(INFO) << "Instancing Objects";
    for (nlohmann::json object : json_data["scene"]["objects"])
    {
        std::string type = object["object"];
        std::string name = object["name"];

        LOG(DEBUG) << "Instancing " << type << " as " << name;

        int index = get_object_index_by_name(type);
        if (index < 0)
        {
            LOG(ERROR) << "Faied to find object " << type << "!";
            exit(-1);
        }

        std::shared_ptr<Object> obj(new Object(*m_objects[index]));

        if (!object["transform"].is_null())
            obj->get_transform()->load(object["transform"].dump());

        obj->set_name(name);
        obj->set_parent(this);

        m_instances.push_back(obj);
    }
    LOG(DEBUG) << "Finished instancing objects,";

    auto time_end = std::chrono::high_resolution_clock::now();

    std::chrono::microseconds time_taken =
        std::chrono::duration_cast<std::chrono::microseconds>(time_end -
                                                              time_begin);

    LOG(INFO) << "Finished loading scene " << m_name << ". Took "
              << time_taken.count() / 1000.f << "ms";
}

////////////////////////////////////////////////////////////////////////////////

void Scene::unload()
{
    LOG(INFO) << "Unloading Scene " << m_name;
    m_objects.clear();
    m_instances.clear();
}

////////////////////////////////////////////////////////////////////////////////

void Scene::setup()
{
    ZoneScopedN("Scene Setup");
    LOG(INFO) << "Setting up Scene " << m_name;

    m_physics_world.setup();

    for (std::shared_ptr<Object> obj : m_instances)
    {
        obj->setup();
    }
    m_setup = true;
}

////////////////////////////////////////////////////////////////////////////////

void Scene::update(std::chrono::microseconds delta_time)
{
    RenderMatrices::get().reset();
    for (std::shared_ptr<Object> obj : m_instances)
    {
        obj->update(delta_time);
    }
}

////////////////////////////////////////////////////////////////////////////////

void Scene::frame() const
{
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (std::shared_ptr<Object> obj : m_instances)
    {
        obj->frame();
    }
}

////////////////////////////////////////////////////////////////////////////////

void Scene::post_update(std::chrono::microseconds delta_time)
{
    for (std::shared_ptr<Object> obj : m_instances)
    {
        obj->post_update(delta_time);
    }
}

////////////////////////////////////////////////////////////////////////////////

void Scene::tick(float tickrate)
{
    m_physics_world.tick(tickrate);

    for (std::shared_ptr<Object> obj : m_instances)
    {
        obj->tick();
    }
}

////////////////////////////////////////////////////////////////////////////////

void Scene::save() const {}

Scene::~Scene() { unload(); }
} // namespace BlueSnow
