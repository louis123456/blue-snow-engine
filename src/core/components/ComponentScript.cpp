#include "core/Object.hpp"
#include "core/Scene.hpp"
#include "core/SceneManager.hpp"
#include "core/components/ComponentScript.hpp"

#include "Tracy.hpp"

namespace BlueSnow
{

ComponentScript::ComponentScript() {}

////////////////////////////////////////////////////////////////////////////////

void ComponentScript::load(const std::string &data)
{
    ZoneScopedN("Script Load");
    nlohmann::json json_data = nlohmann::json::parse(data);

    m_name = json_data["name"];
    m_file = json_data["file"];
}

////////////////////////////////////////////////////////////////////////////////

void ComponentScript::unload()
{
    ZoneScopedN("Script Unload");
    // FIXME segfault
    // m_lua_unload();
}

////////////////////////////////////////////////////////////////////////////////

void ComponentScript::setup()
{
    ZoneScopedN("Script Setup");
    LuaState *lua_state = &LuaState::get();

    m_script_sandbox = lua_state->create_new_sandbox();

    m_script_sandbox["scene"] = m_parent->get_parent();
    m_script_sandbox["scene_manager"] = m_parent->get_parent()->get_parent();
    m_script_sandbox["parent"] = m_parent;

    lua_state->m_luastate.script_file(m_file, m_script_sandbox);

    m_lua_setup = m_script_sandbox["setup"];
    m_lua_update = m_script_sandbox["update"];
    m_lua_tick = m_script_sandbox["tick"];
    m_lua_frame = m_script_sandbox["frame"];
    m_lua_post_update = m_script_sandbox["post_update"];
    m_lua_unload = m_script_sandbox["unload"];

    m_lua_setup();
}

////////////////////////////////////////////////////////////////////////////////

void ComponentScript::update(std::chrono::microseconds delta_time)
{
    ZoneScopedN("Script Update");
    m_lua_update(delta_time.count());
}

////////////////////////////////////////////////////////////////////////////////

void ComponentScript::frame() const
{
    ZoneScopedN("Script frame");
    m_lua_frame();
}

////////////////////////////////////////////////////////////////////////////////

void ComponentScript::post_update(std::chrono::microseconds delta_time)
{
    ZoneScopedN("Script Post Update");
    m_lua_post_update(delta_time.count());
}

////////////////////////////////////////////////////////////////////////////////

std::string ComponentScript::save() const {}

////////////////////////////////////////////////////////////////////////////////

void ComponentScript::tick()
{
    ZoneScopedN("Script Tick");
    m_lua_tick();
}

////////////////////////////////////////////////////////////////////////////////

ComponentScript::~ComponentScript() { unload(); }

} // namespace BlueSnow
