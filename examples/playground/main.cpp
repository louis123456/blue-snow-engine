#include <fstream>
#include <iostream>

#include <json.hpp>

#include <core/Exceptions.hpp>
#include <core/Logger.hpp>
#include <core/RenderMatrices.hpp>
#include <core/Scene.hpp>
#include <core/SceneManager.hpp>
#include <core/events/Event.hpp>
#include <core/init.hpp>
#include <input/Keyboard.hpp>
#include <input/Mouse.hpp>
#include <render/Material.hpp>
#include <render/Shader.hpp>
#include <render/Texture.hpp>
#include <window/RenderWindow.hpp>

#include "tracy/Tracy.hpp"

int main()
{
    BlueSnow::init();
    BlueSnow::WindowSettings settings;
    settings.resolution = {1280, 720};
    settings.title = "Meme";
    settings.fullscreen = false;
    settings.vsync = false;
    BlueSnow::RenderWindow win(settings);

    BlueSnow::InitGL();

    std::ifstream file_mat("assets/materials/materials/test.json");

    if (!file_mat)
    {
        std::cout << "Failed to open file: "
                  << "assets/scenes/pbr_test.json"
                  << "!" << std::endl;
    }
    std::string file_data_mat((std::istreambuf_iterator<char>(file_mat)),
                              std::istreambuf_iterator<char>());
    BlueSnow::Material mat(file_data_mat);

    file_mat.close();
    BlueSnow::SceneManager sm;
    sm.load("assets/manager.json");
    sm.set_tick_target(32.f);

    bool run = true;

    bool load = true;

    while (run)
    {
        BlueSnow::Event ev = win.poll_events();
        if (ev.type == BlueSnow::BLUESNOW_QUIT)
            run = false;
        if (ev.type == BlueSnow::BLUESNOW_WINDOW)
        {
            glViewport(0, 0, ev.window.size.size_x, ev.window.size.size_y);
            BlueSnow::RenderMatrices::get().screen_size = {
                ev.window.size.size_x, ev.window.size.size_y};
        }
        if (BlueSnow::Keyboard::is_key_down(BlueSnow::Keyboard::A) && load)
        {
            sm.pause();
        }
        if (BlueSnow::Keyboard::is_key_down(BlueSnow::Keyboard::B) && load)
        {
            sm.resume();
        }
        mat.use();

        sm.render_loop();

        win.swap_buffers();
    }
    win.close_window();
}