#pragma once

#include "core/events/WindowEvent.hpp"

namespace BlueSnow
{
enum EventTypes
{
    BLUESNOW_WINDOW,
    BLUESNOW_QUIT,
    BLUESNOW_KEYBOARD,
    BLUESNOW_NONE
};

union Event {
    EventTypes type;
    WindowEvent window;
};

} // namespace BlueSnow
