#include <fstream>
#include <iostream>

#include <glad/glad.h>
#include <json.hpp>

#include "core/Logger.hpp"
#include "core/RenderMatrices.hpp"
#include "core/components/ComponentMesh.hpp"
#include "io/BinaryBuffer.hpp"
#include "io/BufferView.hpp"
#include "version.hpp"

namespace BlueSnow
{
ComponentMesh::ComponentMesh() {}

////////////////////////////////////////////////////////////////////////////////

ComponentMesh::ComponentMesh(const std::string &data, BufferView vertices,
                             BufferView normals, BufferView uvs,
                             BufferView indicies)
    : m_vertices(vertices.get_as_vec3()), m_normals(normals.get_as_vec3()),
      m_uvs(uvs.get_as_vec2()), m_indices(indicies.get_as_unsigned_short())
{
    load(data);
}

////////////////////////////////////////////////////////////////////////////////

ComponentMesh::ComponentMesh(const std::string &data) { load(data); }

////////////////////////////////////////////////////////////////////////////////

void ComponentMesh::generate_gl_buffers()
{
    glGenVertexArrays(1, &m_VAO);
    glGenBuffers(1, &m_VBO);
    glGenBuffers(1, &m_NVBO);
    glGenBuffers(1, &m_UVBO);
    glGenBuffers(1, &m_EBO);
}

////////////////////////////////////////////////////////////////////////////////

void ComponentMesh::update_gl_vertex_buffer()
{
    glBindVertexArray(m_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
    glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * 3 * sizeof(float),
                 m_vertices.data(),
                 m_mesh_static ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, m_NVBO);
    glBufferData(GL_ARRAY_BUFFER, m_normals.size() * 3 * sizeof(float),
                 m_normals.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(uint16_t),
                 m_indices.data(), GL_STATIC_DRAW);

    glBindVertexArray(0);
}

////////////////////////////////////////////////////////////////////////////////

void ComponentMesh::update_gl_uv_buffer()
{
    glBindVertexArray(m_VAO);

    glBindBuffer(GL_ARRAY_BUFFER, m_UVBO);
    glBufferData(GL_ARRAY_BUFFER, m_uvs.size() * 2 * sizeof(float),
                 m_uvs.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

////////////////////////////////////////////////////////////////////////////////

void ComponentMesh::load(const std::string &data)
{
    nlohmann::json json_data = nlohmann::json::parse(data);

    m_name = json_data["name"];

    std::string mesh_type = json_data["mesh_type"];
    std::string custom_material;
    if (!json_data["custom_material"].is_null())
        custom_material = json_data["custom_material"];
    else
        custom_material = "";

    if (mesh_type == "static")
        m_mesh_static = true;
    else if (mesh_type == "dynamic")
        m_mesh_static = false;
    else if (mesh_type == "nodraw")
        m_draw_mesh = false;
    else
        LOG(WARN) << "Mesh type not defined for " << m_name
                  << "! Asuming static.";

    translate(glm::vec3(json_data["transform"]["position"][0],
                        json_data["transform"]["position"][1],
                        json_data["transform"]["position"][2]));

    scale(glm::vec3(json_data["transform"]["scale"][0],
                    json_data["transform"]["scale"][1],
                    json_data["transform"]["scale"][2]));

    rotate(glm::vec3(json_data["transform"]["rotation"][0],
                     json_data["transform"]["rotation"][1],
                     json_data["transform"]["rotation"][2]));

    if (!m_draw_mesh)
        return;

    m_loaded = true;
}

////////////////////////////////////////////////////////////////////////////////

void ComponentMesh::unload()
{
    if (m_loaded)
    {
        m_indices.clear();
        m_vertices.clear();
        m_normals.clear();
        m_uvs.clear();
        glDeleteVertexArrays(1, &m_VAO);
        glDeleteBuffers(1, &m_VBO);
        glDeleteBuffers(1, &m_UVBO);
        glDeleteBuffers(1, &m_EBO);
        m_loaded = false;
    }
}

////////////////////////////////////////////////////////////////////////////////

void ComponentMesh::setup()
{
    generate_gl_buffers();
    update_gl_vertex_buffer();
    update_gl_uv_buffer();
}

////////////////////////////////////////////////////////////////////////////////

void ComponentMesh::update(std::chrono::microseconds delta_time) {}

////////////////////////////////////////////////////////////////////////////////

void ComponentMesh::frame() const
{
    if (!m_draw_mesh)
        return;

    glBindVertexArray(m_VAO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);

    RenderMatrices::get().Model = m_transform;

    glUniformMatrix4fv(0, 1, GL_FALSE, &RenderMatrices::get().get_MMO()[0][0]);
    glUniformMatrix4fv(1, 1, GL_FALSE, &RenderMatrices::get().get_VP()[0][0]);

    glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_SHORT, 0);

    RenderMatrices::get().Model = glm::mat4(1.0f);
}

////////////////////////////////////////////////////////////////////////////////

void ComponentMesh::post_update(std::chrono::microseconds delta_time) {}

////////////////////////////////////////////////////////////////////////////////

std::string ComponentMesh::save() const {}

////////////////////////////////////////////////////////////////////////////////

void ComponentMesh::tick() {}

////////////////////////////////////////////////////////////////////////////////

ComponentMesh::~ComponentMesh() { unload(); }

} // namespace BlueSnow
