#pragma once

namespace BlueSnow
{
enum FilterMode
{
    TEXTURE_FILTER_NEAREST,
    TEXTURE_FILTER_LINEAR,
    TEXTURE_MIPMAP_NEAREST,
    TEXTURE_MIPMAP_LINEAR
};
enum WrapMode
{
    TEXTURE_WRAP_REPEAT,
    TEXTURE_WRAP_MIRRORED,
    TEXTURE_WRAP_CLAMP,
    TEXTURE_WRAP_BORDER
};

struct TextureSettings
{
    FilterMode texture_filter_mode_min = TEXTURE_MIPMAP_NEAREST;
    FilterMode texture_filter_mode_mag = TEXTURE_FILTER_NEAREST;

    WrapMode texture_wrap_top = TEXTURE_WRAP_REPEAT;
    WrapMode texture_wrap_side = TEXTURE_WRAP_REPEAT;

    bool genmipmap = true;
};
} // namespace BlueSnow
