#version 330 core
#extension GL_ARB_explicit_uniform_location : require

in vec3 frag_normal;
in vec2 frag_uv;
in vec3 frag_pos;

out vec4 FragColor;

layout(location = 32) uniform sampler2D normal_map;
layout(location = 16) uniform vec3 test;

const vec3 light_pos = vec3(2, 2, 2);

vec3 get_normal()
{
    vec3 normal_sample = texture(normal_map, frag_uv).xyz;

    vec3 tangentNormal = vec3(normal_sample * 2.0 - 1.0);

    vec3 Q1 = dFdx(frag_pos);
    vec3 Q2 = dFdy(frag_pos);
    vec2 st1 = dFdx(frag_uv);
    vec2 st2 = dFdy(frag_uv);

    vec3 N = normalize(frag_normal);
    vec3 T = normalize(Q1 * st2.t - Q2 * st1.t);
    vec3 B = -normalize(cross(N, T));
    mat3 TBN = mat3(T, B, N);

    return normalize(TBN * tangentNormal);
}

void main()
{
    vec3 normal = get_normal();
    vec3 colour =
        (max(dot(normalize(normal), normalize(light_pos - frag_pos)), 0.0) +
         0.3) * test;

    FragColor = vec4(colour, 1.0);
}
   