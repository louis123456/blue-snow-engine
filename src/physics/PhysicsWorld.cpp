#include "physics/PhysicsWorld.hpp"

namespace BlueSnow
{

PhysicsWorld::PhysicsWorld()
{
    m_collision_config = std::make_shared<btDefaultCollisionConfiguration>();

    m_dispatcher = std::make_shared<btCollisionDispatcher>( //
        m_collision_config.get()                            //
    );

    m_broadphase = std::make_shared<btDbvtBroadphase>();

    m_solver = std::make_shared<btSequentialImpulseConstraintSolver>();

    m_dynamics_world = std::make_shared<btDiscreteDynamicsWorld>( //
        m_dispatcher.get(),                                       //
        m_broadphase.get(),                                       //
        m_solver.get(),                                           //
        m_collision_config.get()                                  //
    );

    init();
}

////////////////////////////////////////////////////////////////////////////////

void PhysicsWorld::init()
{
    // FIXME add custom gravity
    m_dynamics_world->setGravity(btVector3(0, -9.81, 0));
}

////////////////////////////////////////////////////////////////////////////////

void PhysicsWorld::register_object(btRigidBody *bt)
{
    m_dynamics_world->addRigidBody(bt);
}

////////////////////////////////////////////////////////////////////////////////

void PhysicsWorld::setup() {}

////////////////////////////////////////////////////////////////////////////////

void PhysicsWorld::tick(float tickrate)
{
    ZoneScopedN("Physics Tick");
    m_dynamics_world->stepSimulation(1.f / tickrate, 10);
}

////////////////////////////////////////////////////////////////////////////////

PhysicsWorld::~PhysicsWorld() {}
} // namespace BlueSnow
