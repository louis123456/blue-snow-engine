#pragma once

namespace BlueSnow
{

enum WindowEventTypes
{
    BLUESNOW_WINDOW_RESIZE
};

struct WindowEvent
{
    WindowEventTypes type;
    struct
    {
        int size_x;
        int size_y;
    } size;
};
} // namespace BlueSnow
