Componernts planned in the engine
=================================

-   ComponentMeshBase -> Mesh represented as vertices + material

    - ComponentMesh -> Mesh represented as vertices + material
    - ComponentAnimatedMesh -> Animated mesh represented as vertices, bones + material.

- ComponentSoundPlayer -> A soundtree player component
- ComponentMusicPlayer -> A music player object
- ComponentListener -> The player’s ‘ears’
- ComponentScript -> A script that can do things to a object
- ComponentNetworking -> How the object serializes and deserializes.
- ComponentPhysicsBase -> A Physics component
- ComponentPhysicsRidgedBody -> A physics RidgedBody.

