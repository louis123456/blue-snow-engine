#include <string>

#include <json.hpp>

#include "core/GlobalSettings.hpp"
#include "core/SceneManager.hpp"

#include "Tracy.hpp"
#include "common/TracySystem.hpp"

namespace BlueSnow
{
SceneManager::SceneManager() {}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::update()
{
    ZoneScopedN("Update");

    std::lock_guard<LockableBase(std::mutex)> lock(m_load_lock);
    std::lock_guard<LockableBase(std::mutex)> lock_1(m_tick_lock);

    m_loaded_scenes.front()->update(m_delta_time);
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::render()
{
    ZoneScopedN("Render");

    std::lock_guard<LockableBase(std::mutex)> lock(m_load_lock);
    std::lock_guard<LockableBase(std::mutex)> lock_1(m_tick_lock);

    m_loaded_scenes.front()->frame();
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::post_update()
{
    ZoneScopedN("Post-Update");

    std::lock_guard<LockableBase(std::mutex)> lock(m_load_lock);
    std::lock_guard<LockableBase(std::mutex)> lock_1(m_tick_lock);

    m_loaded_scenes.front()->post_update(m_delta_time);
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::tick()
{
    ZoneScopedN("Tick");
    std::lock_guard<LockableBase(std::mutex)> lock(m_load_lock);
    std::lock_guard<LockableBase(std::mutex)> lock_1(m_tick_lock);

    m_loaded_scenes.front()->tick(m_target_tps);
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::tick_async()
{
    using clock = std::chrono::high_resolution_clock;
    using namespace std::chrono_literals;

    tracy::SetThreadName("Tick-Thread");

    while (m_tick_thread_run)
    {
        ZoneScopedN("Tick Loop");
        auto tick_interval = 1s / (m_target_tps + 1.f);

        if (clock::now() - m_last_tick >= tick_interval)
        {
            auto start = clock::now();
            m_tps = 1s / (start - m_last_tick);

            tick();

            if (m_average_tps == 0)
            {
                m_average_tps = m_tps;
            }
            m_average_tps = ((m_average_tps * 9) + m_tps) / 10;

            TracyPlot("TPS", m_tps);
            TracyPlot("AVERAGE TPS", m_average_tps);

            auto end = clock::now();
            m_last_tick = end;
            auto wait = (tick_interval - (end - start)) * 0.9f;
            {
                ZoneScopedN("TickWait");
                std::this_thread::sleep_for(wait);
            }
            FrameMarkNamed("Tick");
        }
        else
        {
            ZoneScopedN("TickWait");
            std::this_thread::sleep_for(50us);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::tick_async_start()
{
    ZoneScopedN("Async Tick Start");
    if (m_tick_thread.joinable())
        m_tick_thread.join();

    m_tick_thread_run = true;

    m_tick_thread = std::thread(&SceneManager::tick_async, this);
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::render_loop()
{
    using clock = std::chrono::high_resolution_clock;
    using namespace std::chrono_literals;

    if (m_do_scene_change)
    {
        m_loaded_scenes.pop();
        m_do_scene_change = false;
    }

    if (!m_loaded_scenes.front()->was_setup())
    {
        m_loaded_scenes.front()->setup();
    }

    if (m_running)
    {
        m_delta_time = std::chrono::duration_cast<std::chrono::milliseconds>(
            clock::now() - m_last_frame_finished);
        update();
    }

    render();

    if (m_running)
    {
        post_update();
    }

    m_fps = 1s / (clock::now() - m_last_frame_finished);

    if (m_average_fps == 0)
    {
        m_average_fps = m_fps;
    }
    m_average_fps = ((m_average_fps * 9) + m_fps) / 10;
    m_last_frame_finished = clock::now();
    TracyPlot("FPS", m_fps);
    TracyPlot("AVERAGE FPS", m_average_fps);
    FrameMark;
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::load(const std::string &path)
{
    ZoneScopedN("Load Scene Manager");
    using clock = std::chrono::high_resolution_clock;

    LOG(INFO) << "Loading scene manager";
    std::ifstream file(GlobalSettings::get().working_dir + path);
    nlohmann::json json_data;
    file >> json_data;
    file.close();

    for (std::string index_entry : json_data["index"])
    {
        LOG(INFO) << "Indexing Scene " << index_entry;
        std::ifstream scene_file(GlobalSettings::get().working_dir +
                                 index_entry);

        if (!scene_file)
        {
            LOG(ERROR) << "Could not open scene file " << index_entry
                       << "! Skiping...";
            break;
        }
        nlohmann::json json_scene;
        scene_file >> json_scene;
        scene_file.close();

        if (json_scene["scene"]["name"].is_null())
        {
            LOG(ERROR) << "Could not read scene file " << index_entry
                       << "! Skiping...";
            break;
        }

        m_scene_index[json_scene["scene"]["name"]] = index_entry;
        LOG(INFO) << "Finished indexing Scene " << index_entry;
    }

    m_loaded_scenes.push(std::shared_ptr<Scene>(new Scene()));
    m_loaded_scenes.front()->load(m_scene_index[json_data["startup"]]);
    m_loaded_scenes.front()->set_parent(this);
    m_loaded_scenes.front()->setup();

    m_running = true;
    m_last_frame_finished = clock::now();
    m_last_tick = m_last_frame_finished;

    tick_async_start();
    LOG(INFO) << "Finished loading scene manager";
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::load_scene_async(const std::string &name)
{
    ZoneScopedN("Load Scene Start Async");

    if (m_load_thread.joinable())
        m_load_thread.join();

    m_load_thread = std::thread(&SceneManager::load_scene, this, name);
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::load_scene(const std::string &name)
{
    ZoneScopedN("Load Scene");

    tracy::SetThreadName("Load Thread");

    std::shared_ptr<Scene> scene_temp(new Scene());
    scene_temp->load(m_scene_index[name]);

    std::lock_guard<LockableBase(std::mutex)> lock(m_load_lock);

    m_loaded_scenes.push(scene_temp);

    m_do_scene_change = true;
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::set_tick_target(float tps) { m_target_tps = tps; }

////////////////////////////////////////////////////////////////////////////////

std::shared_ptr<Scene> SceneManager::get_active_scene() const
{
    return m_loaded_scenes.front();
}

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_tick_target() const { return m_target_tps; }

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_current_fps() const { return m_fps; }

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_average_fps() const { return m_global_average_fps; }

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_scene_fps() const { return m_average_fps; }

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_minimum_fps() const { return m_min_fps; }

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_maximum_fps() const { return m_max_fps; }

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_current_tps() const { return m_tps; }

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_average_tps() const { return m_global_average_tps; }

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_scene_tps() const { return m_average_tps; }

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_minimum_tps() const { return m_min_tps; }

////////////////////////////////////////////////////////////////////////////////

float SceneManager::get_maximum_tps() const { return m_max_tps; }

////////////////////////////////////////////////////////////////////////////////

void SceneManager::pause()
{
    if (m_running)
    {
        m_running = false;
        m_tick_thread_run = false;
    }
}

////////////////////////////////////////////////////////////////////////////////

void SceneManager::step() { render_loop(); }

////////////////////////////////////////////////////////////////////////////////

void SceneManager::resume()
{
    if (!m_running)
    {
        using clock = std::chrono::high_resolution_clock;

        m_running = true;
        m_last_frame_finished = clock::now();
        m_last_tick = m_last_frame_finished;

        tick_async_start();
    }
}

////////////////////////////////////////////////////////////////////////////////

SceneManager::~SceneManager()
{
    ZoneScopedN("Unload");
    m_tick_thread_run = false;
    m_tick_thread.join();
    if (m_load_thread.joinable())
        m_load_thread.join();
}
} // namespace BlueSnow
