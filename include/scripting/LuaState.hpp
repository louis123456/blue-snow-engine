#pragma once
#define SOL_ALL_SAFETIES_ON 1

#include <string>

#include "core/Logger.hpp"

#include <sol/sol.hpp>

namespace BlueSnow
{
class LuaState
{
private:
public:
    LuaState() {}
    LuaState(LuaState const &) = delete;
    void operator=(LuaState const &) = delete;

    sol::state m_luastate;

    void init();

    void register_types();

    sol::environment create_new_sandbox();

    static LuaState &get();
};
} // namespace BlueSnow