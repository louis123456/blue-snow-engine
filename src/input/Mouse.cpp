#include <SDL2/SDL.h>

#include "input/Mouse.hpp"

namespace BlueSnow
{
namespace Mouse
{

bool is_button_pressed(MouseButton button)
{
    Uint32 mouse_state = 0;
    if (SDL_GetRelativeMouseMode())
    {
        mouse_state = SDL_GetRelativeMouseState(nullptr, nullptr);
    }
    else
    {
        mouse_state = SDL_GetMouseState(nullptr, nullptr);
    }
    switch (button)
    {
    case MouseButton::Left:
        return mouse_state & SDL_BUTTON(SDL_BUTTON_LEFT);
    case MouseButton::Right:
        return mouse_state & SDL_BUTTON(SDL_BUTTON_RIGHT);
    case MouseButton::Middle:
        return mouse_state & SDL_BUTTON(SDL_BUTTON_MIDDLE);
        break;
    default:
        return false;
        break;
    }
}

////////////////////////////////////////////////////////////////////////////////

glm::vec2i get_cursor_pos()
{
    int x, y = 0;
    if (SDL_GetRelativeMouseMode())
    {
        SDL_GetRelativeMouseState(&x, &y);
    }
    else
    {
        SDL_GetMouseState(&x, &y);
    }
    return glm::vec2i(x, y);
}

////////////////////////////////////////////////////////////////////////////////

void lock_cursor() { SDL_SetRelativeMouseMode(SDL_TRUE); }

////////////////////////////////////////////////////////////////////////////////

void unlock_cursor() { SDL_SetRelativeMouseMode(SDL_FALSE); }

} // namespace Mouse
} // namespace BlueSnow
