#include "io/BufferView.hpp"

#include "core/Logger.hpp"

namespace BlueSnow
{

BufferView::BufferView(BinaryBuffer *data, int offset, int len)
    : m_offset(offset), m_length(len)
{
    m_data = data;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<glm::vec4> BufferView::get_as_vec4() const
{
    assert(m_length % 16 == 0);

    std::vector<glm::vec4> out;
    for (int i = m_offset; i < m_offset + m_length; i += 16)
    {
        out.push_back(
            glm::vec4(*reinterpret_cast<float *>(&m_data->operator[](i + 0)),
                      *reinterpret_cast<float *>(&m_data->operator[](i + 4)),
                      *reinterpret_cast<float *>(&m_data->operator[](i + 8)),
                      *reinterpret_cast<float *>(&m_data->operator[](i + 12))));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<glm::vec3> BufferView::get_as_vec3() const
{
    assert(m_length % 12 == 0);

    std::vector<glm::vec3> out;
    for (int i = m_offset; i < m_offset + m_length; i += 12)
    {
        out.push_back(
            glm::vec3(*reinterpret_cast<float *>(&m_data->operator[](i + 0)),
                      *reinterpret_cast<float *>(&m_data->operator[](i + 4)),
                      *reinterpret_cast<float *>(&m_data->operator[](i + 8))));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<glm::vec2> BufferView::get_as_vec2() const
{
    assert(m_length % 8 == 0);

    std::vector<glm::vec2> out;
    for (int i = m_offset; i < m_offset + m_length; i += 8)
    {
        out.push_back(
            glm::vec2(*reinterpret_cast<float *>(&m_data->operator[](i + 0)),
                      *reinterpret_cast<float *>(&m_data->operator[](i + 4))));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<int16_t> BufferView::get_as_short() const
{
    assert(m_length % 2 == 0);

    std::vector<int16_t> out;
    for (int i = m_offset; i < m_offset + m_length; i += 2)
    {
        out.push_back(*reinterpret_cast<int16_t *>(&m_data->operator[](i + 0)));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<int32_t> BufferView::get_as_int() const
{
    assert(m_length % 4 == 0);

    std::vector<int32_t> out;
    for (int i = m_offset; i < m_offset + m_length; i += 4)
    {
        out.push_back(*reinterpret_cast<int32_t *>(&m_data->operator[](i + 0)));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<int64_t> BufferView::get_as_int64() const
{
    assert(m_length % 8 == 0);

    std::vector<int64_t> out;
    for (int i = m_offset; i < m_offset + m_length; i += 8)
    {
        out.push_back(*reinterpret_cast<int64_t *>(&m_data->operator[](i + 0)));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<uint16_t> BufferView::get_as_unsigned_short() const
{
    assert(m_length % 2 == 0);

    std::vector<uint16_t> out;
    for (int i = m_offset; i < m_offset + m_length; i += 2)
    {
        out.push_back(
            *reinterpret_cast<uint16_t *>(&m_data->operator[](i + 0)));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<uint32_t> BufferView::get_as_unsigned_int() const
{
    assert(m_length % 4 == 0);

    std::vector<uint32_t> out;
    for (int i = m_offset; i < m_offset + m_length; i += 4)
    {
        out.push_back(
            *reinterpret_cast<uint32_t *>(&m_data->operator[](i + 0)));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<uint64_t> BufferView::get_as_unsigned_int64() const
{
    assert(m_length % 8 == 0);

    std::vector<uint64_t> out;
    for (int i = m_offset; i < m_offset + m_length; i += 8)
    {
        out.push_back(
            *reinterpret_cast<uint64_t *>(&m_data->operator[](i + 0)));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<float> BufferView::get_as_float() const
{
    assert(m_length % 4 == 0);

    std::vector<float> out;
    for (int i = m_offset; i < m_offset + m_length; i += 4)
    {
        out.push_back(*reinterpret_cast<float *>(&m_data->operator[](i + 0)));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<double> BufferView::get_as_double() const
{
    assert(m_length % 8 == 0);

    std::vector<double> out;
    for (int i = m_offset; i < m_offset + m_length; i += 8)
    {
        out.push_back(*reinterpret_cast<double *>(&m_data->operator[](i + 0)));
    }
    return out;
}

////////////////////////////////////////////////////////////////////////////////

std::vector<unsigned char> BufferView::get_as_bytes() const
{
    assert(m_length % 1 == 0);

    std::vector<unsigned char> out;
    for (int i = m_offset; i < m_offset + m_length; i += 1)
    {
        out.push_back(
            *reinterpret_cast<unsigned char *>(&m_data->operator[](i + 0)));
    }
    return out;
}

} // namespace BlueSnow
