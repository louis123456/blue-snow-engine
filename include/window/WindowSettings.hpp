/**
 * \file WindowSettings.hpp
 * \author Louis van der Walt
 * \brief The structure required by Window for settings.
 * \date 2019-12-23
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include <glm/glm.hpp>
#include <string>

namespace BlueSnow
{
/**
 * \struct WindowSettings
 *
 * \brief The settings used to create a Window
 *
 */
struct WindowSettings
{
    struct
    {
        int size_x = 800; ///< Size of the Window in the X axis.
        int size_y = 600; ///< Size of the Window in the Y axis.
    } resolution;         ///< Size of the Window.

    bool fullscreen = false;
    bool resizeable = false;

    bool vsync = false;

    uint8_t context_version_major = 3;
    uint8_t context_version_minor = 3;

    std::string title = "BlueSnowWindow"; ///< The Window title.
};
} // namespace BlueSnow