#version 330 core
#extension GL_ARB_explicit_uniform_location : require

layout(location = 0) in vec3 Pos;
layout(location = 1) in vec3 Normal;
layout(location = 2) in vec2 UV;

layout(location = 0) uniform mat4 MMO;
layout(location = 1) uniform mat4 VP;

out vec3 frag_normal;
out vec3 frag_pos;
out vec2 frag_uv;

void main()
{
    frag_normal = mat3(transpose(inverse(MMO))) * Normal;
    frag_pos = vec3(MMO * vec4(Pos, 1.0));
    frag_uv = UV;

    gl_Position = VP * MMO * vec4(Pos, 1.0);
}