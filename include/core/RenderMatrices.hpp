#pragma once

#include <glm/glm.hpp>

namespace BlueSnow
{
class RenderMatrices
{
private:
public:
    RenderMatrices() {}
    RenderMatrices(RenderMatrices const &) = delete;
    void operator=(RenderMatrices const &) = delete;

    glm::vec2 screen_size = glm::vec2(1280, 720);

    glm::mat4 Model = glm::mat4(1.0f);
    glm::mat4 Object = glm::mat4(1.0f);
    glm::mat4 View = glm::mat4(1.0f);
    glm::mat4 Perspective = glm::mat4(1.0f);

    float get_screen_aspect() { return screen_size.x / screen_size.y; }

    void reset()
    {
        Model = glm::mat4(1.0f);
        Object = glm::mat4(1.0f);
        View = glm::mat4(1.0f);
        Perspective = glm::mat4(1.0f);
    }

    glm::mat4 get_MMO() { return Object * Model; }
    glm::mat4 get_VP() { return Perspective * View; }

    static RenderMatrices &get()
    {
        static RenderMatrices inst;
        return inst;
    }
};
} // namespace BlueSnow
