#include <thread>

#include <glad/glad.h>
#include <json.hpp>

#include "core/RenderMatrices.hpp"
#include "core/components/ComponentCamera.hpp"
#include "input/Keyboard.hpp"

namespace BlueSnow
{

ComponentCamera::ComponentCamera() {}

////////////////////////////////////////////////////////////////////////////////

void ComponentCamera::load(const std::string &data)
{
    nlohmann::json json_data = nlohmann::json::parse(data);

    m_name = json_data["name"];

    m_fov = json_data["perspective_fov"];

    m_clip_far = json_data["clip_far"];
    m_clip_near = json_data["clip_near"];

    translate(glm::vec3(json_data["transform"]["position"][0],
                        json_data["transform"]["position"][1],
                        json_data["transform"]["position"][2]));

    scale(glm::vec3(json_data["transform"]["scale"][0],
                    json_data["transform"]["scale"][1],
                    json_data["transform"]["scale"][2]));

    if (!json_data["transform"]["rotation"].is_null())
    {
        rotate(glm::vec3(json_data["transform"]["rotation"][0],
                         json_data["transform"]["rotation"][1],
                         json_data["transform"]["rotation"][2]));
    }
    if (!json_data["transform"]["quat"].is_null())
    {
        rotate(glm::quat(json_data["transform"]["quat"][0],
                         json_data["transform"]["quat"][1],
                         json_data["transform"]["quat"][2],
                         json_data["transform"]["quat"][3]));
    }
    m_transform = inverse(m_transform);
}

////////////////////////////////////////////////////////////////////////////////

void ComponentCamera::ComponentCamera::unload() {}

////////////////////////////////////////////////////////////////////////////////

void ComponentCamera::setup() {}

////////////////////////////////////////////////////////////////////////////////

void ComponentCamera::update(std::chrono::microseconds delta_time)
{
    glm::mat4 projection = glm::perspective(
        glm::radians(m_fov), RenderMatrices::get().get_screen_aspect(),
        m_clip_near, m_clip_far);

    RenderMatrices::get().Perspective = projection;
    RenderMatrices::get().View = m_transform;
}

////////////////////////////////////////////////////////////////////////////////

void ComponentCamera::frame() const {}

////////////////////////////////////////////////////////////////////////////////

void ComponentCamera::post_update(std::chrono::microseconds delta_time) {}

////////////////////////////////////////////////////////////////////////////////

std::string ComponentCamera::save() const {}

////////////////////////////////////////////////////////////////////////////////

void ComponentCamera::tick() {}

////////////////////////////////////////////////////////////////////////////////

ComponentCamera::~ComponentCamera() {}

} // namespace BlueSnow
