#pragma once

#include <chrono>
#include <fstream>
#include <map>
#include <mutex>
#include <queue>
#include <string>
#include <thread>

#include "core/Logger.hpp"
#include "core/Scene.hpp"
#include "core/SettingsManager.hpp"

#include "Tracy.hpp"

namespace BlueSnow
{
class SceneManager
{
private:
    std::atomic_bool m_tick_thread_run =
        true;                           ///< Weather to run the tick thread
    std::atomic_bool m_tick_run = true; ///< Weather to run the tick loop

    std::thread m_tick_thread; ///< The tick loop's thread
    std::thread m_load_thread; ///< The Loading thread

    TracyLockable(std ::mutex, m_tick_lock);
    TracyLockable(std ::mutex, m_load_lock);

    std::map<std::string, std::string> m_scene_index; ///< A list of all scenes

    std::queue<std::shared_ptr<Scene>>
        m_loaded_scenes; ///< A list of pointers to all loaded scenes

    std::atomic_bool m_do_scene_change =
        false; ///< should pop and unload front of loaded scenes

    bool m_running = false; ///< Is the loop running

    float m_target_tps = 0.0f; ///< The target TPS
    float m_tps = 0.0f;        ///< The current TPS
    float m_min_tps = 0.0f;    ///< The Minumum TPS
    float m_max_tps = 0.0f;    ///< The maximum TPS

    float m_average_tps = 0.0f;        ///< The Average TPS.
    float m_global_average_tps = 0.0f; ///< The Global Average TPS.

    float m_fps = 0.0f;             ///< The current FPS
    float m_min_fps = 100000000.0f; ///< The minumum FPS
    float m_max_fps = 0.0f;         ///< The maximum FPS

    float m_average_fps = 0.0f;        ///< The Average FPS.
    float m_global_average_fps = 0.0f; ///< The Global Average FPS.

    std::chrono::high_resolution_clock::time_point
        m_last_frame_finished; ///< The time the last frame finished
    std::chrono::high_resolution_clock::time_point
        m_last_tick; ///< The time of the last tick

    std::chrono::microseconds m_delta_time =
        std::chrono::microseconds(0); ///< The delta time

    /**
     * \brief Update the scene
     *
     */
    void update();

    /**
     * \brief Render the scene
     *
     */
    void render();

    /**
     * \brief Called after the rendering of the scene
     *
     */
    void post_update();

    /**
     * \brief Do a tick
     *
     */
    void tick();

    /**
     * \brief Async tick loop
     *
     */
    void tick_async();

    /**
     * \brief Start tick loop
     *
     */
    void tick_async_start();

public:
    /**
     * \brief Constructs a Scene Manager
     *
     */
    SceneManager();

    /**
     * \brief The render loop
     *
     */
    void render_loop();

    /**
     * \brief Loads the manager settings from file.
     *
     * \param path Path to .json file with data
     */
    void load(const std::string &path);

    /**
     * \brief Loads a scene asyncrounously
     *
     * \param name The name of the scene.
     */
    void load_scene_async(const std::string &name);

    /**
     * \brief Loads a scene and sets it as active
     *
     * \param name The name of the scene.
     */
    void load_scene(const std::string &name);

    /**
     * \brief Sets the tick target
     *
     * \param tps The target amount of ticks per seccond.
     */
    void set_tick_target(float tps);

    /**
     * \brief Gets a pointer to the active scene
     *
     * \return Scene* A pointer to the active scene.
     */
    std::shared_ptr<Scene> get_active_scene() const;

    /**
     * \brief Gets the tick target
     *
     * \return float
     */
    float get_tick_target() const;

    /**
     * \brief Gets the fps of the last seccond in the current scene
     *
     * \return float The fps of last seccond
     */
    float get_current_fps() const;

    /**
     * \brief Gets the average fps sincce launce
     *
     * \return float The average fps of global
     */
    float get_average_fps() const;

    /**
     * \brief Gets the scene fps of the current scene
     *
     * \return float The average fps of current loaded scene
     */
    float get_scene_fps() const;

    /**
     * \brief Gets the minimum fps of the current scene
     *
     * \return float The minimum fps of the current scene
     */
    float get_minimum_fps() const;

    /**
     * \brief Get the maximum fps of the current scene
     *
     * \return float The maximum fps of the current scene
     */
    float get_maximum_fps() const;

    /**
     * \brief Gets the fps of the last seccond in the current scene
     *
     * \return float The fps of last seccond
     */
    float get_current_tps() const;

    /**
     * \brief Gets the average tps since launch
     *
     * \return float The average tps of global
     */
    float get_average_tps() const;

    /**
     * \brief Gets the scene tps of the current scene
     *
     * \return float The average tps of current loaded scene
     */
    float get_scene_tps() const;

    /**
     * \brief Gets the minimum tps of the current scene
     *
     * \return float The minimum tps of the current scene
     */
    float get_minimum_tps() const;

    /**
     * \brief Get the maximum tps of the current scene
     *
     * \return float The maximum rps of the current scene
     */
    float get_maximum_tps() const;

    /**
     * \brief Pause the current scene updates and tick.
     *
     */
    void pause();

    /**
     * \brief Step updates of current scene
     *
     */
    void step();

    /**
     * \brief Resume the current scene updates and tick.
     *
     */
    void resume();

    /**
     * \brief Destroy the Scene Manager
     *
     */
    ~SceneManager();
};
} // namespace BlueSnow
