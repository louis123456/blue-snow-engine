#pragma once

#include <glm/glm.hpp>

#include "core/math/vecint.hpp"
#include "input/MouseButtons.hpp"

namespace BlueSnow
{
namespace Mouse
{
/**
 * \brief Check if mouse button in pressed.
 *
 * \param button The /ref MouseButton to check
 * \return true Pressed
 * \return false Released
 */
bool is_button_pressed(MouseButton button);

/**
 * \brief Get the cursor position
 *
 * \return glm::vec<2, int>
 */
glm::vec2i get_cursor_pos();

/**
 * \brief Hide and lock the mouse
 *
 */
void lock_cursor();

/**
 * \brief Show and unlock the mouse
 *
 */
void unlock_cursor();
} // namespace Mouse
} // namespace BlueSnow
