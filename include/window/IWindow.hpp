/**
 * \file Window.hpp
 */
#pragma once

#include <glad/glad.h>

#include "core/events/Event.hpp"
#include "window/WindowSettings.hpp"

namespace BlueSnow
{
/**
 * \class Window Interface
 *
 * \brief Creates a window with the platform specific api
 */
class IWindow
{
private:
public:
    /**
     * \brief Creates the Window, called by the Constructor aswell.
     *
     * \param settings the Settings used in the creation of the Window.
     */
    virtual void create(const WindowSettings &settings) = 0;

    /**
     * \brief Checks For and returns events
     *
     * \return WindowEvent
     */
    virtual Event poll_events() = 0;

    /**
     * \brief Swap the buffers of the Window
     *
     */
    virtual void swap_buffers() = 0;
};
} // namespace BlueSnow