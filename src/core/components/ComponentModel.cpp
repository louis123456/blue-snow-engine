#include "core/RenderMatrices.hpp"
#include "core/components/ComponentModel.hpp"
namespace BlueSnow
{

ComponentModel::ComponentModel() {}

////////////////////////////////////////////////////////////////////////////////

void ComponentModel::load_json_chunk(const std::vector<unsigned char> &data)
{
    std::string json_string;
    for (unsigned char char_in : data)
    {
        json_string.push_back(*reinterpret_cast<char *>(&char_in));
    }

    nlohmann::json json_data = nlohmann::json::parse(json_string);

    if (json_data["asset"]["version"] != "2.0")
    {
        LOG(ERROR) << "Invalid glTF file, only version 2 supported! Attempting "
                      "anyway.";
    }

    m_file_json_data = json_data;
}

////////////////////////////////////////////////////////////////////////////////

void ComponentModel::load_binary_chunk(const std::vector<unsigned char> &data)
{
    m_buffers.push_back(data);
}

////////////////////////////////////////////////////////////////////////////////

void ComponentModel::load_meshes()
{
    // Load Buffer Views
    for (nlohmann::json buffer_view_data : m_file_json_data["bufferViews"])
    {
        int buffer = buffer_view_data["buffer"];
        int len = buffer_view_data["byteLength"];
        int offset = buffer_view_data["byteOffset"];

        m_buffer_views.push_back(BufferView(&m_buffers[buffer], offset, len));
    }

    for (nlohmann::json meshes : m_file_json_data["meshes"])
    {
        std::string mesh_name = meshes["name"];
        LOG(DEBUG) << "Loading mesh " << mesh_name;

        nlohmann::json primitive = meshes["primitives"][0];

        int pos = primitive["attributes"]["POSITION"];
        int normal = primitive["attributes"]["NORMAL"];
        int uvs = primitive["attributes"]["TEXCOORD_0"];
        int indices = primitive["indices"];

        int pos_view = m_file_json_data["accessors"][pos]["bufferView"];
        int normal_view = m_file_json_data["accessors"][normal]["bufferView"];
        int uvs_view = m_file_json_data["accessors"][uvs]["bufferView"];
        int indices_view = m_file_json_data["accessors"][indices]["bufferView"];

        nlohmann::json mesh_data;

        // TODO Add proper transform here.
        mesh_data["name"] = mesh_name;
        mesh_data["mesh_type"] = "static";
        mesh_data["transform"]["position"] = {0.0, 0.0, 0.0};
        mesh_data["transform"]["rotation"] = {0.0, 0.0, 0.0};
        mesh_data["transform"]["scale"] = {1.0, 1.0, 1.0};

        m_mesh_data.push_back(std::make_shared<ComponentMesh>( //
            mesh_data.dump(),                                  //
            m_buffer_views[pos_view],                          //
            m_buffer_views[normal_view],                       //
            m_buffer_views[uvs_view],                          //
            m_buffer_views[indices_view]                       //
            ));
    }
}
////////////////////////////////////////////////////////////////////////////////

void ComponentModel::load_gltf(const std::string &path)
{
    LOG(DEBUG) << "Loading " << path;
    std::ifstream file(path, std::ios::binary);

    file.unsetf(std::ios::skipws);

    std::streampos file_size;

    file.seekg(0, std::ios::end);
    file_size = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<unsigned char> vec;
    vec.reserve(file_size);

    // read the data:
    vec.insert(vec.begin(), std::istream_iterator<unsigned char>(file),
               std::istream_iterator<unsigned char>());
    file.close();

    BinaryBuffer raw_file_data;
    raw_file_data.set_data(vec);

    uint32_t magic, version, length;
    magic = *reinterpret_cast<uint32_t *>(&raw_file_data[0]);
    version = *reinterpret_cast<uint32_t *>(&raw_file_data[4]);
    length = *reinterpret_cast<uint32_t *>(&raw_file_data[8]);

    if (magic != 0x46546C67 || version != 2)
    {
        LOG(ERROR) << "Invalid glTF binary file." << path
                   << ". Only GLB version 2 supported!";
        exit(-1);
    }

    size_t cursor_location = 12;

    while (cursor_location < length)
    {
        std::vector<unsigned char> chunk_data;
        uint32_t chunk_length, chunk_type;

        chunk_length =
            *reinterpret_cast<uint32_t *>(&raw_file_data[cursor_location + 0]);
        chunk_type =
            *reinterpret_cast<uint32_t *>(&raw_file_data[cursor_location + 4]);

        cursor_location += 8;
        BufferView chunk_data_buffer_view(&raw_file_data, cursor_location,
                                          chunk_length);

        chunk_data = chunk_data_buffer_view.get_as_bytes();

        switch (chunk_type)
        {
        case 0x4E4F534A:
            load_json_chunk(chunk_data);
            break;
        case 0x004E4942:
            load_binary_chunk(chunk_data);
        default:
            break;
        }
        cursor_location += chunk_length;
    }
    load_meshes();

    LOG(DEBUG) << "Finished loading" << path;
}

////////////////////////////////////////////////////////////////////////////////

void ComponentModel::load(const std::string &data)
{
    nlohmann::json json_data = nlohmann::json::parse(data);

    m_name = json_data["name"];

    std::string file = json_data["file"];

    load_gltf(file);
}

////////////////////////////////////////////////////////////////////////////////

void ComponentModel::ComponentModel::unload() { m_mesh_data.clear(); }

////////////////////////////////////////////////////////////////////////////////

void ComponentModel::setup()
{
    for (auto mesh : m_mesh_data)
    {
        mesh->setup();
    }
}

////////////////////////////////////////////////////////////////////////////////

void ComponentModel::update(std::chrono::microseconds delta_time)
{
    for (auto mesh : m_mesh_data)
    {
        mesh->update(delta_time);
    }
}

////////////////////////////////////////////////////////////////////////////////

void ComponentModel::frame() const
{
    for (auto mesh : m_mesh_data)
    {
        mesh->frame();
    }
    RenderMatrices::get().Model = glm::mat4(1.0f);
}

////////////////////////////////////////////////////////////////////////////////

void ComponentModel::post_update(std::chrono::microseconds delta_time)
{
    for (auto mesh : m_mesh_data)
    {
        mesh->post_update(delta_time);
    }
}

////////////////////////////////////////////////////////////////////////////////

std::string ComponentModel::save() const {}

////////////////////////////////////////////////////////////////////////////////

void ComponentModel::tick()
{
    for (auto mesh : m_mesh_data)
    {
        mesh->tick();
    }
}

////////////////////////////////////////////////////////////////////////////////

ComponentModel::~ComponentModel() { unload(); }

} // namespace BlueSnow
