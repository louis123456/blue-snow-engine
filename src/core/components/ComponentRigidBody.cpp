#include "core/components/ComponentRigidBody.hpp"

#include "core/Object.hpp"
#include "core/Scene.hpp"

namespace BlueSnow
{

ComponentRigidBody::ComponentRigidBody() {}

////////////////////////////////////////////////////////////////////////////////

ComponentRigidBody *ComponentRigidBody::clone() const
{
    ComponentRigidBody *clone = new ComponentRigidBody(*this);
    clone->m_collision_shape = this->m_collision_shape;

    return clone;
}

////////////////////////////////////////////////////////////////////////////////

void ComponentRigidBody::load(const std::string &data)
{
    nlohmann::json json_data = nlohmann::json::parse(data);

    m_name = json_data["name"];
    m_mass = json_data["mass"];

    // FIXME error checking
    m_collision_shape = std::make_shared<btBoxShape>(
        btVector3(json_data["collider"]["box"][0], //
                  json_data["collider"]["box"][1], //
                  json_data["collider"]["box"][2]  //
                  ));
}

////////////////////////////////////////////////////////////////////////////////

void ComponentRigidBody::ComponentRigidBody::unload() {}

////////////////////////////////////////////////////////////////////////////////

void ComponentRigidBody::setup()
{
    bool isDynamic = (m_mass != 0.f);

    btVector3 local_inertia(0, 0, 0);
    if (isDynamic)
        m_collision_shape->calculateLocalInertia(m_mass, local_inertia);

    m_parent_transform = m_parent->get_transform();

    glm::vec3 pos = m_parent_transform->get_position();
    glm::quat rot = m_parent_transform->get_rotation_quaternion();

    btTransform transform;
    transform.setIdentity();
    transform.setRotation(btQuaternion(rot.x, rot.y, rot.z, rot.w));
    transform.setOrigin(btVector3(pos.x, pos.y, pos.z));

    m_motion_state = std::make_shared<btDefaultMotionState>(transform);

    btRigidBody::btRigidBodyConstructionInfo rbInfo(
        m_mass, m_motion_state.get(), m_collision_shape.get(), local_inertia);

    m_rigid_body = std::make_shared<btRigidBody>(rbInfo);

    m_parent->get_parent()->get_physics_world().register_object(
        m_rigid_body.get());
}

////////////////////////////////////////////////////////////////////////////////

void ComponentRigidBody::update(std::chrono::microseconds delta_time) {}

////////////////////////////////////////////////////////////////////////////////

void ComponentRigidBody::frame() const {}

////////////////////////////////////////////////////////////////////////////////

void ComponentRigidBody::post_update(std::chrono::microseconds delta_time) {}

////////////////////////////////////////////////////////////////////////////////

std::string ComponentRigidBody::save() const {}

////////////////////////////////////////////////////////////////////////////////

void ComponentRigidBody::tick()
{
    btTransform trans;
    if (m_rigid_body && m_rigid_body->getMotionState())
    {
        m_rigid_body->getMotionState()->getWorldTransform(trans);
    }

    m_parent->get_transform()->set_position(glm::vec3( //
        float(trans.getOrigin().getX()),               //
        float(trans.getOrigin().getY()),               //
        float(trans.getOrigin().getZ())                //
        ));

    m_parent->get_transform()->set_rotation(glm::quat( //
        float(trans.getRotation().getW()),             //
        float(trans.getRotation().getX()),             //
        float(trans.getRotation().getY()),             //
        float(trans.getRotation().getZ())              //
        ));

    printf("world pos object = %f,%f,%f\n", float(trans.getOrigin().getX()),
           float(trans.getOrigin().getY()), float(trans.getOrigin().getZ()));
}

////////////////////////////////////////////////////////////////////////////////

ComponentRigidBody::~ComponentRigidBody() {}

} // namespace BlueSnow
