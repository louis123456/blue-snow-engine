#include "render/Material.hpp"

#include "Tracy.hpp"

namespace BlueSnow
{
Material::Material() {}

////////////////////////////////////////////////////////////////////////////////

void Material::load(const std::string &data)
{

    ZoneScopedN("load_material");

    nlohmann::json json_data = nlohmann::json::parse(data);

    m_int_uniforms.clear();
    m_uint_uniforms.clear();
    m_float_uniforms.clear();
    m_double_uniforms.clear();
    m_vec2_uniforms.clear();
    m_vec3_uniforms.clear();
    m_vec4_uniforms.clear();
    m_mat3_uniforms.clear();
    m_mat4_uniforms.clear();

    m_name = json_data["Material"]["name"];
    std::string vert_shader = json_data["Material"]["shader"]["vert"];
    std::string frag_shader = json_data["Material"]["shader"]["frag"];

    m_shader.load_file(vert_shader, frag_shader);
    m_shader.compile_and_link();

    if (json_data["Material"]["textures"].size() > 16)
    {
        LOG(ERROR) << "Only maximum 16 (0-15) textures supported per Material!";
        exit(-1);
    }

    for (nlohmann::json texture : json_data["Material"]["textures"])
    {
        TextureSettings settings;
        settings.genmipmap = texture["mipmap"];
        if (settings.genmipmap)
            settings.texture_filter_mode_min = TEXTURE_MIPMAP_LINEAR;
        else
            settings.texture_filter_mode_min = TEXTURE_FILTER_LINEAR;

        m_textures.push_back(new Texture(texture["file"], settings));
    }

    for (nlohmann::json uniform : json_data["Material"]["variables"])
    {
        int type = uniform["type"];

        switch (type)
        {
        case UNIFORM_TEXTURE:
        {
            int location = uniform["location"];
            m_int_uniforms.push_back(
                std::make_pair(location, uniform["value"]));
            break;
        }
        case UNIFORM_INT:
        {
            m_int_uniforms.push_back(
                std::make_pair(uniform["location"], uniform["value"]));
            break;
        }
        case UNIFORM_UINT:
        {
            m_uint_uniforms.push_back(
                std::make_pair(uniform["location"], uniform["value"]));
            break;
        }
        case UNIFORM_FLOAT:
        {
            m_float_uniforms.push_back(
                std::make_pair(uniform["location"], uniform["value"]));
            break;
        }
        case UNIFORM_DOUBLE:
        {
            m_double_uniforms.push_back(
                std::make_pair(uniform["location"], uniform["value"]));
            break;
        }
        case UNIFORM_VEC2:
        {
            glm::vec2 vector(uniform["value"][0], uniform["value"][1]);
            m_vec2_uniforms.push_back(
                std::make_pair(uniform["location"], vector));
            break;
        }
        case UNIFORM_VEC3:
        {
            glm::vec3 vector(uniform["value"][0], uniform["value"][1],
                             uniform["value"][2]);
            m_vec3_uniforms.push_back(
                std::make_pair(uniform["location"], vector));
            break;
        }
        case UNIFORM_VEC4:
        {
            glm::vec4 vector(uniform["value"][0], uniform["value"][1],
                             uniform["value"][2], uniform["value"][3]);
            m_vec3_uniforms.push_back(
                std::make_pair(uniform["location"], vector));
            break;
        }
        case UNIFORM_MAT3:
            // m_mat3_uniforms.push_back(std::make_pair(uniform["location"],
            // uniform["value"]));
            break;
        case UNIFORM_MAT4:
            // m_mat4_uniforms.push_back(std::make_pair(uniform["location"],
            // uniform["value"]));
            break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

void Material::use()
{
    m_shader.use();
    for (int i = 0; i < m_textures.size(); i++)
    {
        m_textures[i]->bind(i);
    }
    for (std::pair<int, int> uniform : m_int_uniforms)
    {
        glUniform1i(uniform.first, uniform.second);
    }
    for (std::pair<int, uint> uniform : m_uint_uniforms)
    {
        glUniform1ui(uniform.first, uniform.second);
    }
    for (std::pair<int, float> uniform : m_float_uniforms)
    {
        glUniform1f(uniform.first, uniform.second);
    }
    for (std::pair<int, double> uniform : m_double_uniforms)
    {
        glUniform1d(uniform.first, uniform.second);
    }
    for (std::pair<int, glm::vec2> uniform : m_vec2_uniforms)
    {
        glUniform2f(uniform.first, uniform.second[0], uniform.second[1]);
    }
    for (std::pair<int, glm::vec3> uniform : m_vec3_uniforms)
    {
        glUniform3f(uniform.first, uniform.second[0], uniform.second[1],
                    uniform.second[2]);
    }
    for (std::pair<int, glm::vec4> uniform : m_vec4_uniforms)
    {
        glUniform4f(uniform.first, uniform.second[0], uniform.second[1],
                    uniform.second[2], uniform.second[3]);
    }
    for (std::pair<int, glm::mat3> uniform : m_mat3_uniforms)
    {
        glUniformMatrix3fv(uniform.first, 1, GL_FALSE, &uniform.second[0][0]);
    }
    for (std::pair<int, glm::mat4> uniform : m_mat4_uniforms)
    {
        glUniformMatrix4fv(uniform.first, 1, GL_FALSE, &uniform.second[0][0]);
    }
}

////////////////////////////////////////////////////////////////////////////////

void Material::unload()
{
    for (int i = 0; i < m_textures.size(); i++)
    {
        m_textures[i]->unload();
        delete m_textures[i];
    }
    m_textures.clear();
}

////////////////////////////////////////////////////////////////////////////////

Material::~Material() { unload(); }
} // namespace BlueSnow
