#pragma once

#include <exception>
#include <string>

namespace BlueSnow
{
struct CommonException : std::exception
{
    std::string m_exception_string;
    explicit CommonException(const std::string &exception_string)
        : m_exception_string(exception_string){};
    const char *what() const throw() { return "Failed to Create a Window"; }
};
} // namespace BlueSnow
