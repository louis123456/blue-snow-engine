#pragma once

#include <glm/glm.hpp>

#include "io/BinaryBuffer.hpp"

namespace BlueSnow
{
class BufferView
{
private:
    BinaryBuffer *m_data;

    int m_offset = 0;
    int m_length = 0;

public:
    BufferView(BinaryBuffer *data, int offset, int len);

    /**
     * \brief Gets the data as a vec4 vector.
     *
     * \return std::vector<glm::vec4>
     */
    std::vector<glm::vec4> get_as_vec4() const;

    /**
     * \brief Gets the data as a vec3 vector.
     *
     * \return std::vector<glm::vec3>
     */
    std::vector<glm::vec3> get_as_vec3() const;

    /**
     * \brief Gets the data as a vec2 vector.
     *
     * \return std::vector<glm::vec2>
     */
    std::vector<glm::vec2> get_as_vec2() const;

    /**
     * \brief Gets the data as a short vector.
     *
     * \return std::vector<int16_t>
     */
    std::vector<int16_t> get_as_short() const;

    /**
     * \brief Gets the data as an int vector.
     *
     * \return std::vector<int32_t>
     */
    std::vector<int32_t> get_as_int() const;

    /**
     * \brief Gets the data as an int64 vector.
     *
     * \return std::vector<int64_t>
     */
    std::vector<int64_t> get_as_int64() const;

    /**
     * \brief Gets the data as an unsigned short vector.
     *
     * \return std::vector<uint16_t>
     */
    std::vector<uint16_t> get_as_unsigned_short() const;

    /**
     * \brief Gets the data as an unsigned int vector.
     *
     * \return std::vector<uint32_t>
     */
    std::vector<uint32_t> get_as_unsigned_int() const;

    /**
     * \brief Gets the data as an unsigned int64 vector.
     *
     * \return std::vector<uint64_t>
     */
    std::vector<uint64_t> get_as_unsigned_int64() const;

    /**
     * \brief Get the data as a float vector.
     *
     * \return std::vector<float>
     */
    std::vector<float> get_as_float() const;

    /**
     * \brief Get the data as a double vector.
     *
     * \return std::vector<double>
     */
    std::vector<double> get_as_double() const;

    /**
     * \brief Gets the data as a vector of bytes.
     *
     * \return std::vector<unsigned char>
     */
    std::vector<unsigned char> get_as_bytes() const;
};
} // namespace BlueSnow
