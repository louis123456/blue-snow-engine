#pragma once

#include <chrono>
#include <memory>
#include <string>

#include <btBulletDynamicsCommon.h>
#include <json.hpp>

#include "core/Transform.hpp"
#include "core/components/Component.hpp"
#include "core/components/ComponentTransform.hpp"

namespace BlueSnow
{
class ComponentRigidBody : public Component
{
private:
    std::shared_ptr<btCollisionShape> m_collision_shape;
    std::shared_ptr<btDefaultMotionState> m_motion_state;
    std::shared_ptr<btRigidBody> m_rigid_body;

    btScalar m_mass = 0.;

    std::shared_ptr<ComponentTransform> m_parent_transform;

public:
    ComponentRigidBody();

    /**
     * \brief Construct a new Rigid Body
     *
     * \param data
     */
    explicit ComponentRigidBody(const std::string &data) { load(data); }

    /**
     * \brief Clones the Component
     *
     * \return
     */
    ComponentRigidBody *clone() const;

    /**
     * \brief Get the type
     *
     * \return std::string
     */
    std::string get_type() override { return "RigidBody"; }

    /**
     * \brief Loads all data from json passed into data
     *
     * \param data The json string to load from
     */
    void load(const std::string &data) override;

    /**
     * \brief Unload all data
     *
     */
    void unload() override;
    /**
     * \brief Called on startup
     *
     */
    void setup() override;

    /**
     * \brief Called before every frame.
     *
     * \param delta_time Time passed since last call
     */
    void update(std::chrono::microseconds delta_time) override;

    /**
     * \brief Draw the frame.
     *
     */
    void frame() const override;

    /**
     * \brief Called after each frame.
     *
     * \param delta_time
     */
    void post_update(std::chrono::microseconds delta_time) override;

    /**
     * \brief Creates a json string with Object data.
     *
     */
    std::string save() const override;

    /**
     * \brief Called at a constant rate.
     *
     */
    void tick();

    ~ComponentRigidBody();
};
} // namespace BlueSnow
