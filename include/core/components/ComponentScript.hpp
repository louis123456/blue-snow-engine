#pragma once

#include <chrono>
#include <string>

#include <json.hpp>
#include <sol/sol.hpp>

#include "core/Transform.hpp"
#include "core/components/Component.hpp"
#include "scripting/LuaState.hpp"

namespace BlueSnow
{
class ComponentScript : public Component
{
private:
    sol::environment m_script_sandbox;

    std::string m_file;

    sol::function m_lua_setup;
    sol::function m_lua_update;
    sol::function m_lua_tick;
    sol::function m_lua_frame;
    sol::function m_lua_post_update;
    sol::function m_lua_unload;

public:
    ComponentScript();

    explicit ComponentScript(const std::string &data) { load(data); }

    /**
     * \brief Clones the Component
     *
     * \return
     */
    ComponentScript *clone() const
    {
        ComponentScript *clone_obj = new ComponentScript();

        clone_obj->m_file = this->m_file;
        clone_obj->m_name = this->m_name;

        return clone_obj;
    }

    /**
     * \brief Get the type
     *
     * \return std::string
     */
    std::string get_type() { return "Script"; }

    /**
     * \brief Loads all data from json passed into data
     *
     * \param data The json string to load from
     */
    void load(const std::string &data);

    /**
     * \brief Unload all data
     *
     */
    void unload();
    /**
     * \brief Called on startup
     *
     */
    void setup();

    /**
     * \brief Called before every frame.
     *
     * \param delta_time Time passed since last call
     */
    void update(std::chrono::microseconds delta_time);

    /**
     * \brief Draw the frame.
     *
     */
    void frame() const;

    /**
     * \brief Called after each frame.
     *
     * \param delta_time
     */
    void post_update(std::chrono::microseconds delta_time);

    /**
     * \brief Creates a json string with Object data.
     *
     */
    std::string save() const;

    /**
     * \brief Called at a constant rate.
     *
     */
    void tick();

    ~ComponentScript();
};
} // namespace BlueSnow
