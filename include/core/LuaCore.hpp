#pragma once

#include <sol/sol.hpp>

#include "core/Object.hpp"
#include "core/Scene.hpp"
#include "core/SceneManager.hpp"

namespace BlueSnow
{
inline void register_core_lua(sol::state &m_luastate)
{
    m_luastate.new_usertype<Transform>(                              //
        "Transform",                                                 //
        "new", sol::no_constructor,                                  //
        "translate", &Transform::translate,                          //
        "rotate", sol::resolve<void(glm::vec3)>(&Transform::rotate), //
        "scale", &Transform::scale                                   //
    );
    m_luastate.new_usertype<Object>(            //
        "Object",                               //
        "new", sol::no_constructor,             //
        "get_name", &Object::get_name,          //
        "get_transform", &Object::get_transform //
    );

    m_luastate.new_usertype<Scene>(                  //
        "Scene",                                     //
        "new", sol::no_constructor,                  //
        "get_name", &Scene::get_name,                //
        "get_instance", &Scene::get_instance_by_name //
    );

    m_luastate.new_usertype<SceneManager>(                  //
        "SceneManager",                                     //
        "new", sol::no_constructor,                         //
        "load_scene_async", &SceneManager::load_scene_async //
    );

    m_luastate.new_usertype<Component>( //
        "Component",                    //
        "new", sol::no_constructor,     //
        "get_name", &Component::get_name);

    m_luastate.new_usertype<ComponentTransform>(              //
        "ComponentTransform",                                 //
        "new", sol::no_constructor,                           //
        sol::base_classes, sol::bases<Transform, Component>() //
    );

    m_luastate.new_usertype<glm::vec3>(                                   //
        "vec3",                                                           //
        sol::constructors<glm::vec3(), glm::vec3(float, float, float)>(), //
        "x", &glm::vec3::x,                                               //
        "y", &glm::vec3::y,                                               //
        "z", &glm::vec3::z                                                //
    );
}
} // namespace BlueSnow
