#include "core/Transform.hpp"

namespace BlueSnow
{
glm::mat4 Transform::get_transform() const { return m_transform; }

////////////////////////////////////////////////////////////////////////////////

void Transform::set_transform(glm::mat4 transform) { m_transform = transform; }

////////////////////////////////////////////////////////////////////////////////

void Transform::set_position(glm::vec3 pos)
{
    m_transform[3] = glm::vec4(pos, 1.f);
}

////////////////////////////////////////////////////////////////////////////////

void Transform::set_rotation(glm::vec3 rot)
{
    glm::vec3 rotation = get_rotation();

    rotation += rot - rotation;

    rotate(rotation);
}

////////////////////////////////////////////////////////////////////////////////

void Transform::set_rotation(glm::quat rot)
{
    glm::quat rotation = get_rotation_quaternion();

    rotation += rot - rotation;

    rotate(rotation);
}

////////////////////////////////////////////////////////////////////////////////

void Transform::set_scale(glm::vec3 scale_in)
{
    glm::vec3 scalex = get_scale();

    scalex += scale_in - scalex;

    scale(scalex);
}

////////////////////////////////////////////////////////////////////////////////

void Transform::translate(glm::vec3 pos)
{
    m_transform = glm::translate(m_transform, pos);
}

////////////////////////////////////////////////////////////////////////////////

void Transform::rotate(glm::vec3 rot)
{
    m_transform = glm::rotate(m_transform, glm::radians(rot.x),
                              glm::vec3(1.0f, 0.0f, 0.0f));
    m_transform = glm::rotate(m_transform, glm::radians(rot.y),
                              glm::vec3(0.0f, 1.0f, 0.0f));
    m_transform = glm::rotate(m_transform, glm::radians(rot.z),
                              glm::vec3(0.0f, 0.0f, 1.0f));
}

////////////////////////////////////////////////////////////////////////////////

void Transform::rotate(glm::quat rot)
{
    glm::vec3 euler = glm::eulerAngles(rot);
    rotate(glm::vec3(euler.y, euler.x, euler.z));
}

////////////////////////////////////////////////////////////////////////////////

void Transform::scale(glm::vec3 scale_in)
{
    m_transform = glm::scale(m_transform, scale_in);
}

////////////////////////////////////////////////////////////////////////////////

glm::vec3 Transform::get_position() const { return m_transform[3]; }

////////////////////////////////////////////////////////////////////////////////

glm::vec3 Transform::get_rotation() const
{
    glm::vec3 scalex;
    glm::quat orientation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_transform, scalex, orientation, translation, skew,
                   perspective);
    glm::vec3 euler = glm::eulerAngles(orientation);
    return glm::vec3(euler.y, euler.x, euler.z);
}

////////////////////////////////////////////////////////////////////////////////

glm::quat Transform::get_rotation_quaternion() const
{
    glm::vec3 scalex;
    glm::quat orientation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_transform, scalex, orientation, translation, skew,
                   perspective);
    return orientation;
}

////////////////////////////////////////////////////////////////////////////////

glm::vec3 Transform::get_scale() const
{
    glm::vec3 scalex;
    glm::quat orientation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(m_transform, scalex, orientation, translation, skew,
                   perspective);
    return scalex;
}
} // namespace BlueSnow
