Variables for materials
=======================

0 = Texture
1 = int
2 = uint
3 = float
4 = double
5 = vec2
6 = vec3
7 = vec4
8 = mat3
9 = mat4
