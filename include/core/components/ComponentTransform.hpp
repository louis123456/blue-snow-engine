#pragma once

#include <chrono>
#include <string>

#include <json.hpp>

#include "core/Transform.hpp"
#include "core/components/Component.hpp"

namespace BlueSnow
{
class ComponentTransform : public Component, public Transform
{
private:
public:
    ComponentTransform();

    explicit ComponentTransform(const std::string &data) { load(data); }

    /**
     * \brief Clones the Component
     *
     * \return
     */
    ComponentTransform *clone() const { return new ComponentTransform(*this); }

    /**
     * \brief Get the type
     *
     * \return std::string
     */
    std::string get_type() { return "Transform"; }

    /**
     * \brief Loads all data from json passed into data
     *
     * \param data The json string to load from
     */
    void load(const std::string &data);

    /**
     * \brief Unload all data
     *
     */
    void unload();
    /**
     * \brief Called on startup
     *
     */
    void setup();

    /**
     * \brief Called before every frame.
     *
     * \param delta_time Time passed since last call
     */
    void update(std::chrono::microseconds delta_time);

    /**
     * \brief Draw the frame.
     *
     */
    void frame() const;

    /**
     * \brief Called after each frame.
     *
     * \param delta_time
     */
    void post_update(std::chrono::microseconds delta_time);

    /**
     * \brief Creates a json string with Object data.
     *
     */
    std::string save() const;

    /**
     * \brief Called at a constant rate.
     *
     */
    void tick();

    ~ComponentTransform();
};
} // namespace BlueSnow
