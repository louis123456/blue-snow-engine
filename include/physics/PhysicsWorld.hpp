#pragma once

#include <memory>

#include <Tracy.hpp>
#include <btBulletDynamicsCommon.h>

namespace BlueSnow
{
class PhysicsWorld
{
private:
    std::shared_ptr<btDefaultCollisionConfiguration> m_collision_config;
    std::shared_ptr<btCollisionDispatcher> m_dispatcher;
    std::shared_ptr<btBroadphaseInterface> m_broadphase;
    std::shared_ptr<btSequentialImpulseConstraintSolver> m_solver;
    std::shared_ptr<btDiscreteDynamicsWorld> m_dynamics_world;

public:
    /**
     * \brief Constructs a new Physics World
     *
     */
    PhysicsWorld();

    /**
     * \brief Init the physics world
     *
     */
    void init();

    void register_object(btRigidBody *rb);

    /**
     * \brief Setup the physics world
     *
     */
    void setup();

    /**
     * \brief Run a simulation tick of length 1 seccond / tickrate
     *
     * \param tickrate The tickrate configured.
     */
    void tick(float tickrate);

    /**
     * \brief Destroys a Physics World object
     *
     */
    ~PhysicsWorld();
};
} // namespace BlueSnow
