#pragma once

#include <glad/glad.h>

#include "scripting/LuaState.hpp"

namespace BlueSnow
{
/**
 * \brief Init BlueSnow engine
 *
 */
void init();

/**
 * \brief Function to Init and Load OpenGL with
 * <a href="https://glad.dav1d.de/">Glad</a>.
 *
 */
void InitGL();
} // namespace BlueSnow
